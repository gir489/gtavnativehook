#include "dllmain.h"

char dlldir[FILENAME_MAX];

ofstream LogFile(XorStr("D:\\GTA_Debug.txt"));
ofstream EventFile(XorStr("D:\\GTA_Events.txt"));

Log::Log(char* filename)
{
    m_stream.open(GetDirectoryFile(filename));
}

Log::~Log()
{
    m_stream.close();
}

char *Log::GetDirectoryFile(char *filename)
{
    static char path[FILENAME_MAX];
    strcpy_s(path, dlldir);
    strcat_s(path, filename);
    return path;
}

void Log::WriteToFile(LPCSTR fmt, ...)
{
    if (!LogFile || !fmt)
        return;

    va_list va_alist;
    char logbuf[2048] = { 0 };

    va_start(va_alist, fmt);
    _vsnprintf_s(logbuf + strlen(logbuf), sizeof(logbuf), sizeof(logbuf) - strlen(logbuf), fmt, va_alist);
    va_end(va_alist);

    struct tm current_tm;
    time_t current_time = time(NULL);
    char szTimestamp[30];
    localtime_s(&current_tm, &current_time);
    sprintf_s(szTimestamp, XorStr("[%02d:%02d:%02d] "), current_tm.tm_hour, current_tm.tm_min, current_tm.tm_sec);

    LogFile << szTimestamp << logbuf << endl;
}

void Log::LogEvent(LPCSTR fmt, ...)
{
    if (!EventFile || !fmt)
        return;

    va_list va_alist;
    char logbuf[2048] = { 0 };

    va_start(va_alist, fmt);
    _vsnprintf_s(logbuf + strlen(logbuf), sizeof(logbuf), sizeof(logbuf) - strlen(logbuf), fmt, va_alist);
    va_end(va_alist);

    struct tm current_tm;
    time_t current_time = time(NULL);
    char szTimestamp[30];
    localtime_s(&current_tm, &current_time);
    sprintf_s(szTimestamp, XorStr("[%02d:%02d:%02d] "), current_tm.tm_hour, current_tm.tm_min, current_tm.tm_sec);

    EventFile << szTimestamp << logbuf << endl;
}

Log* pLog;