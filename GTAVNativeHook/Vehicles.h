#pragma once

enum Boats
{
    VEHICLE_DINGHY = 0x3D961290,
    VEHICLE_DINGHY2 = 0x107F392C,
    VEHICLE_DINGHY3 = 0x1E5E54EA,
    VEHICLE_DINGHY4 = 0x33B47F96,
    VEHICLE_JETMAX = 0x33581161,
    VEHICLE_MARQUIS = 0xC1CE1183,
    VEHICLE_SEASHARK = 0xC2974024,
    VEHICLE_SEASHARK2 = 0xDB4388E4,
    VEHICLE_SEASHARK3 = 0xED762D49,
    VEHICLE_SPEEDER = 0xDC60D2B,
    VEHICLE_SPEEDER2 = 0x1A144F2A,
    VEHICLE_SQUALO = 0x17DF5EC2,
    VEHICLE_SUBMERSIBLE = 0x2DFF622F,
    VEHICLE_SUBMERSIBLE2 = 0xC07107EE,
    VEHICLE_SUNTRAP = 0xEF2295C9,
    VEHICLE_TORO = 0x3FD5AA2F,
    VEHICLE_TORO2 = 0x362CAC6D,
    VEHICLE_TROPIC = 0x1149422F,
    VEHICLE_TROPIC2 = 0x56590FE9,
    VEHICLE_TUG = 0x82CAC433
};

enum Commericals
{
    VEHICLE_BENSON = 0x7A61B330,
    VEHICLE_BIFF = 0x32B91AE8,
    VEHICLE_HAULER = 0x5A82F9AE,
    VEHICLE_HAULER2 = 0x171C92C4,
    VEHICLE_MULE = 0x35ED670B,
    VEHICLE_MULE2 = 0xC1632BEB,
    VEHICLE_MULE3 = 0x85A5B471,
    VEHICLE_MULE4 = 0x73F4110E,
    VEHICLE_PACKER = 0x21EEE87D,
    VEHICLE_PHANTOM = 0x809AA4CB,
    VEHICLE_PHANTOM2 = 0x9DAE1398,
    VEHICLE_PHANTOM3 = 0xA90ED5C,
    VEHICLE_POUNDER = 0x7DE35E7D,
    VEHICLE_POUNDER2 = 0x6290F15B,
    VEHICLE_STOCKADE = 0x6827CF72,
    VEHICLE_STOCKADE3 = 0xF337AB36,
    VEHICLE_TERBYTE = 0x897AFC65
};

enum Compacts
{
    VEHICLE_BLISTA = 0xEB70965F,
    VEHICLE_BRIOSO = 0x5C55CB39,
    VEHICLE_DILETTANTE = 0xBC993509,
    VEHICLE_DILETTANTE2 = 0x64430650,
    VEHICLE_ISSI2 = 0xB9CB3B69,
    VEHICLE_ISSI3 = 0x378236E1,
    VEHICLE_PANTO = 0xE644E480,
    VEHICLE_PRAIRIE = 0xA988D3A2,
    VEHICLE_RHAPSODY = 0x322CF98F
};

enum Coupes
{
    VEHICLE_COGCABRIO = 0x13B57D8A,
    VEHICLE_EXEMPLAR = 0xFFB15B5E,
    VEHICLE_F620 = 0xDCBCBE48,
    VEHICLE_FELON = 0xE8A8BDA8,
    VEHICLE_FELON2 = 0xFAAD85EE,
    VEHICLE_JACKAL = 0xDAC67112,
    VEHICLE_ORACLE = 0x506434F6,
    VEHICLE_ORACLE2 = 0xE18195B2,
    VEHICLE_SENTINEL = 0x50732C82,
    VEHICLE_SENTINEL2 = 0x3412AE2D,
    VEHICLE_WINDSOR = 0x5E4327C8,
    VEHICLE_WINDSOR2 = 0x8CF5CAE1,
    VEHICLE_ZION = 0xBD1B39C3,
    VEHICLE_ZION2 = 0xB8E2AE18
};

enum Bikes
{
    VEHICLE_BMX = 0x43779C54,
    VEHICLE_CRUISER = 0x1ABA13B5,
    VEHICLE_FIXTER = 0xCE23D3BF,
    VEHICLE_SCORCHER = 0xF4E1AA15,
    VEHICLE_TRIBIKE = 0x4339CD69,
    VEHICLE_TRIBIKE2 = 0xB67597EC,
    VEHICLE_TRIBIKE3 = 0xE823FB48
};

enum Emergency
{
    VEHICLE_AMBULANCE = 0x45D56ADA,
    VEHICLE_FBI = 0x432EA949,
    VEHICLE_FBI2 = 0x9DC66994,
    VEHICLE_FIRETRUK = 0x55702DD8,
    VEHICLE_LGUARD = 0x1BF8D381,
    VEHICLE_PBUS = 0x885F3671,
    VEHICLE_POLICE = 0x79FBB0C5,
    VEHICLE_POLICE2 = 0x9F05F101,
    VEHICLE_POLICE3 = 0x71FA16EA,
    VEHICLE_POLICE4 = 0x8A63C7B9,
    VEHICLE_POLICEB = 0xFDEFAEC3,
    VEHICLE_POLMAV = 0x1517D4D9,
    VEHICLE_POLICEOLD1 = 0xA46462F7,
    VEHICLE_POLICEOLD2 = 0x95F4C618,
    VEHICLE_POLICET = 0x1B38E955,
    VEHICLE_PRANGER = 0x2C33B46E,
    VEHICLE_PREDATOR = 0xE2E7D4AB,
    VEHICLE_RIOT = 0xB822A1AA,
    VEHICLE_RIOT2 = 0x9B16A3B4,
    VEHICLE_SHERIFF = 0x9BAA707C,
    VEHICLE_SHERIFF2 = 0x72935408
};

enum Helicopters
{
    VEHICLE_AKULA = 0x46699F47,
    VEHICLE_ANNIHILATOR = 0x31F0B376,
    VEHICLE_BUZZARD = 0x2F03547B,
    VEHICLE_BUZZARD2 = 0x2C75F0DD,
    VEHICLE_CARGOBOB = 0xFCFCB68B,
    VEHICLE_CARGOBOB2 = 0x60A7EA10,
    VEHICLE_CARGOBOB3 = 0x53174EEF,
    VEHICLE_CARGOBOB4 = 0x78BC1A3C,
    VEHICLE_FROGGER = 0x2C634FBD,
    VEHICLE_FROGGER2 = 0x742E9AC0,
    VEHICLE_HAVOK = 0x89BA59F5,
    VEHICLE_HUNTER = 0xFD707EDE,
    VEHICLE_MAVERICK = 0x9D0450CA,
    VEHICLE_SAVAGE = 0xFB133A17,
    VEHICLE_SKYLIFT = 0x3E48BF23,
    VEHICLE_SUPERVOLITO = 0x2A54C47D,
    VEHICLE_SUPERVOLITO2 = 0x9C5E5644,
    VEHICLE_SWIFT = 0xEBC24DF2,
    VEHICLE_SWIFT2 = 0x4019CB4C,
    VEHICLE_VALKYRIE = 0xA09E15FD,
    VEHICLE_VALKYRIE2 = 0x5BFA5C4B,
    VEHICLE_VOLATUS = 0x920016F1
};

enum Industrial
{
    VEHICLE_BULLDOZER = 0x7074F39D,
    VEHICLE_CUTTER = 0xC3FBA120,
    VEHICLE_DUMP = 0x810369E2,
    VEHICLE_FLATBED = 0x50B0215A,
    VEHICLE_GUARDIAN = 0x825A9F4C,
    VEHICLE_HANDLER = 0x1A7FCEFA,
    VEHICLE_MIXER = 0xD138A6BB,
    VEHICLE_MIXER2 = 0x1C534995,
    VEHICLE_RUBBLE = 0x9A5B1DCC,
    VEHICLE_TIPTRUCK = 0x2E19879,
    VEHICLE_TIPTRUCK2 = 0xC7824E5E,
};

enum Military
{
    VEHICLE_APC = 0x2189D250,
    VEHICLE_BARRACKS = 0xCEEA3F4B,
    VEHICLE_BARRACKS2 = 0x4008EABB,
    VEHICLE_BARRACKS3 = 0x2592B5CF,
    VEHICLE_BARRAGE = 0xF34DFB25,
    VEHICLE_CHERNOBOG = 0xD6BC7523,
    VEHICLE_CRUSADER = 0x132D5A1A,
    VEHICLE_HALFTRACK = 0xFE141DA6,
    VEHICLE_KHANJALI = 0xAA6F980A,
    VEHICLE_RHINO = 0x2EA68690,
    VEHICLE_THRUSTER = 0x58CDAF30,
    VEHICLE_TRAILERSMALL2 = 0x8FD54EBB
};

enum Motorcycles
{
    VEHICLE_AKUMA = 0x63ABADE7,
    VEHICLE_AVARUS = 0x81E38F7F,
    VEHICLE_BAGGER = 0x806B9CC3,
    VEHICLE_BATI = 0xF9300CC5,
    VEHICLE_BATI2 = 0xCADD5D2D,
    VEHICLE_BF400 = 0x5283265,
    VEHICLE_CARBONRS = 0xABB0C0,
    VEHICLE_CHIMERA = 0x675ED7,
    VEHICLE_CLIFFHANGER = 0x17420102,
    VEHICLE_DAEMON = 0x77934CEE,
    VEHICLE_DAEMON2 = 0xAC4E93C9,
    VEHICLE_DEFILER = 0x30FF0190,
    VEHICLE_DIABLOUS = 0xF1B44F44,
    VEHICLE_DIABLOUS2 = 0x6ABDF65E,
    VEHICLE_DOUBLE = 0x9C669788,
    VEHICLE_ENDURO = 0x6882FA73,
    VEHICLE_ESSKEY = 0x794CB30C,
    VEHICLE_FAGGIO = 0x9229E4EB,
    VEHICLE_FAGGIO2 = 0x350D1AB,
    VEHICLE_FAGGIO3 = 0xB328B188,
    VEHICLE_FCR = 0x25676EAF,
    VEHICLE_FCR2 = 0xD2D5E00E,
    VEHICLE_GARGOYLE = 0x2C2C2324,
    VEHICLE_HAKUCHOU = 0x4B6C568A,
    VEHICLE_HAKUCHOU2 = 0xF0C2A91F,
    VEHICLE_HEXER = 0x11F76C14,
    VEHICLE_INNOVATION = 0xF683EACA,
    VEHICLE_LECTRO = 0x26321E67,
    VEHICLE_MANCHEZ = 0xA5325278,
    VEHICLE_NEMESIS = 0xDA288376,
    VEHICLE_NIGHTBLADE = 0xA0438767,
    VEHICLE_OPPRESSOR = 0x34B82784,
    VEHICLE_OPPRESSOR2 = 0x7B54A9D3,
    VEHICLE_PCJ = 0xC9CEAF06,
    VEHICLE_RATBIKE = 0x6FACDF31,
    VEHICLE_RUFFIAN = 0xCABD11E8,
    VEHICLE_SANCHEZ2 = 0xA960B13E,
    VEHICLE_SANCTUS = 0x58E316C7,
    VEHICLE_SHOTARO = 0xE7D2A16E,
    VEHICLE_SOVEREIGN = 0x2C509634,
    VEHICLE_THRUST = 0x6D6F8F43,
    VEHICLE_VADER = 0xF79A00F7,
    VEHICLE_VINDICATOR = 0xAF599F01,
    VEHICLE_VORTEX = 0xDBA9DBFC,
    VEHICLE_WOLFSBANE = 0xDB20A373,
    VEHICLE_ZOMBIEA = 0xC3D7C72B,
    VEHICLE_ZOMBIEB = 0xDE05FB87
};

enum Muscle
{
    VEHICLE_BLADE = 0xB820ED5E,
    VEHICLE_BUCCANEER = 0xD756460C,
    VEHICLE_BUCCANEER2 = 0xC397F748,
    VEHICLE_CHINO = 0x14D69010,
    VEHICLE_CHINO2 = 0xAED64A63,
    VEHICLE_COQUETTE3 = 0x2EC385FE,
    VEHICLE_DOMINATOR = 0x4CE68AC,
    VEHICLE_DOMINATOR2 = 0xC96B73D9,
    VEHICLE_DOMINATOR3 = 0xC52C6B93,
    VEHICLE_DUKES = 0x2B26F456,
    VEHICLE_DUKES2 = 0xEC8F7094,
    VEHICLE_ELLIE = 0xB472D2B5,
    VEHICLE_FACTION = 0x81A9CDDF,
    VEHICLE_FACTION2 = 0x95466BDB,
    VEHICLE_FACTION3 = 0x866BCE26,
    VEHICLE_GAUNTLET = 0x94B395C5,
    VEHICLE_GAUNTLET2 = 0x14D22159,
    VEHICLE_HERMES = 0xE83C17,
    VEHICLE_HOTKNIFE = 0x239E390,
    VEHICLE_LURCHER = 0x7B47A6A7,
    VEHICLE_MOONBEAM = 0x1F52A43F,
    VEHICLE_MOONBEAM2 = 0x710A2B9B,
    VEHICLE_NIGHTSHADE = 0x8C2BD0DC,
    VEHICLE_PHOENIX = 0x4E3BEDF7,
    VEHICLE_PICADOR = 0x59E0FBF3,
    VEHICLE_RATLOADER = 0xD83C13CE,
    VEHICLE_RATLOADER2 = 0xDCE1D9F7,
    VEHICLE_RUINER = 0xF26CEFF9,
    VEHICLE_RUINER2 = 0x381E10BD,
    VEHICLE_RUINER3 = 0x2E5AFD37,
    VEHICLE_SABREGT = 0x9B909C94,
    VEHICLE_SABREGT2 = 0xD4EA603,
    VEHICLE_SLAMVAN = 0x2B7F9DE3,
    VEHICLE_SLAMVAN2 = 0x31ADBBFC,
    VEHICLE_SLAMVAN3 = 0x42BC5E19,
    VEHICLE_STALION = 0x72A4C31E,
    VEHICLE_STALION2 = 0xE80F67EE,
    VEHICLE_TAMPA = 0x39F9C898,
    VEHICLE_TAMPA3 = 0xB7D9F7F1,
    VEHICLE_VIGERO = 0xCEC6B9B7,
    VEHICLE_VIRGO = 0xE2504942,
    VEHICLE_VIRGO2 = 0xCA62927A,
    VEHICLE_VIRGO3 = 0xFDFFB0,
    VEHICLE_VOODOO = 0x779B4F2D,
    VEHICLE_VOODOO2 = 0x1F3766E3,
    VEHICLE_YOSEMITE = 0x6F946279,
};

enum OffRoad
{
    VEHICLE_BFINJECTION = 0x432AA566,
    VEHICLE_BIFTA = 0xEB298297,
    VEHICLE_BLAZER = 0x8125BCF9,
    VEHICLE_BLAZER2 = 0xFD231729,
    VEHICLE_BLAZER3 = 0xB44F0582,
    VEHICLE_BLAZER4 = 0xE5BA6858,
    VEHICLE_BLAZER5 = 0xA1355F67,
    VEHICLE_BODHI2 = 0xAA699BB6,
    VEHICLE_BRAWLER = 0xA7CE1BC5,
    VEHICLE_DLOADER = 0x698521E3,
    VEHICLE_DUBSTA3 = 0xB6410173,
    VEHICLE_DUNE = 0x9CF21E0F,
    VEHICLE_DUNE2 = 0x1FD824AF,
    VEHICLE_DUNE3 = 0x711D4738,
    VEHICLE_DUNE4 = 0xCEB28249,
    VEHICLE_DUNE5 = 0xED62BFA9,
    VEHICLE_FREECRAWLER = 0xFCC2F483,
    VEHICLE_INSURGENT = 0x9114EADA,
    VEHICLE_INSURGENT2 = 0x7B7E56F0,
    VEHICLE_INSURGENT3 = 0x8D4B7A8A,
    VEHICLE_KALAHARI = 0x5852838,
    VEHICLE_KAMACHO = 0xF8C2E0E7,
    VEHICLE_MARSHALL = 0x49863E9C,
    VEHICLE_MESA3 = 0x84F42E51,
    VEHICLE_MONSTER = 0xCD93A7DB,
    VEHICLE_MENACER = 0x79DD18AE,
    VEHICLE_NIGHTSHARK = 0x19DD9ED1,
    VEHICLE_RANCHERXL = 0x6210CBB0,
    VEHICLE_RANCHERXL2 = 0x7341576B,
    VEHICLE_REBEL = 0xB802DD46,
    VEHICLE_REBEL2 = 0x8612B64B,
    VEHICLE_RIATA = 0xA4A4E453,
    VEHICLE_SANDKING = 0xB9210FD0,
    VEHICLE_SANDKING2 = 0x3AF8C345,
    VEHICLE_TECHNICAL = 0x83051506,
    VEHICLE_TECHNICAL2 = 0x4662BCBB,
    VEHICLE_TECHNICAL3 = 0x50D4D19F,
    VEHICLE_TROPHYTRUCK = 0x612F4B6,
    VEHICLE_TROPHYTRUCK2 = 0xD876DBE2,
};

enum Planes
{
    VEHICLE_ALPHAZ1 = 0xA52F6866,
    VEHICLE_AVENGER = 0x81BD2ED0,
    VEHICLE_BESRA = 0x6CBD1D6D,
    VEHICLE_BLIMP = 0xF7004C86,
    VEHICLE_BLIMP2 = 0xDB6B4924,
    VEHICLE_BLIMP3 = 0xEDA4ED97,
    VEHICLE_BOMBUSHKA = 0xFE0A508C,
    VEHICLE_CARGOPLANE = 0x15F27762,
    VEHICLE_CUBAN800 = 0xD9927FE3,
    VEHICLE_DODO = 0xCA495705,
    VEHICLE_DUSTER = 0x39D6779E,
    VEHICLE_HOWARD = 0xC3F25753,
    VEHICLE_HYDRA = 0x39D6E83F,
    VEHICLE_JET = 0x3F119114,
    VEHICLE_LAZER = 0xB39B0AE6,
    VEHICLE_LUXOR = 0x250B0C5E,
    VEHICLE_LUXOR2 = 0xB79F589E,
    VEHICLE_MAMMATUS = 0x97E55D11,
    VEHICLE_MICROLIGHT = 0x96E24857,
    VEHICLE_MILJET = 0x9D80F93,
    VEHICLE_MOGUL = 0xD35698EF,
    VEHICLE_MOLOTOK = 0x5D56F01B,
    VEHICLE_NIMBUS = 0xB2CF7250,
    VEHICLE_NOKOTA = 0x3DC92356,
    VEHICLE_PYRO = 0xAD6065C0,
    VEHICLE_ROGUE = 0xC5DD6967,
    VEHICLE_SEABREEZE = 0xE8983F9F,
    VEHICLE_SHAMAL = 0xB79C1BF5,
    VEHICLE_STARLING = 0x9A9EB7DE,
    VEHICLE_STRIKEFORCE = 0x64DE07A1,
    VEHICLE_STUNT = 0x81794C70,
    VEHICLE_TITAN = 0x761E2AD3,
    VEHICLE_TULA = 0x3E2E4F8A,
    VEHICLE_VELUM = 0x9C429B6A,
    VEHICLE_VELUM2 = 0x403820E8,
    VEHICLE_VESTRA = 0x4FF77E37,
    VEHICLE_VOLATOL = 0x1AAD0DED
};

enum SUVs
{
    VEHICLE_BALLER = 0xCFCA3668,
    VEHICLE_BALLER2 = 0x8852855,
    VEHICLE_BALLER3 = 0x6FF0F727,
    VEHICLE_BALLER4 = 0x25CBE2E2,
    VEHICLE_BALLER5 = 0x1C09CF5E,
    VEHICLE_BALLER6 = 0x27B4E6B0,
    VEHICLE_BJXL = 0x32B29A4B,
    VEHICLE_CAVALCADE = 0x779F23AA,
    VEHICLE_CAVALCADE2 = 0xD0EB2BE5,
    VEHICLE_CONTENDER = 0x28B67ACA,
    VEHICLE_DUBSTA = 0x462FE277,
    VEHICLE_DUBSTA2 = 0xE882E5F6,
    VEHICLE_FQ2 = 0xBC32A33B,
    VEHICLE_GRANGER = 0x9628879C,
    VEHICLE_GRESLEY = 0xA3FC0F4D,
    VEHICLE_HABANERO = 0x34B7390F,
    VEHICLE_HUNTLEY = 0x1D06D681,
    VEHICLE_LANDSTALKER = 0x4BA4E8DC,
    VEHICLE_MESA = 0x36848602,
    VEHICLE_MESA2 = 0xD36A4B44,
    VEHICLE_PATRIOT = 0xCFCFEB3B,
    VEHICLE_PATRIOT2 = 0xE6E967F8,
    VEHICLE_RADI = 0x9D96B45B,
    VEHICLE_ROCOTO = 0x7F5C91F1,
    VEHICLE_SEMINOLE = 0x48CECED3,
    VEHICLE_SERRANO = 0x4FB1A214,
    VEHICLE_XLS = 0x47BBCF2E,
    VEHICLE_XLS2 = 0xE6401328
};

enum Sedans
{
    VEHICLE_ASEA = 0x94204D89,
    VEHICLE_ASEA2 = 0x9441D8D5,
    VEHICLE_ASTEROPE = 0x8E9254FB,
    VEHICLE_CHEBUREK = 0xC514AAE0,
    VEHICLE_COG55 = 0x360A438E,
    VEHICLE_COG552 = 0x29FCD3E4,
    VEHICLE_COGNOSCENTI = 0x86FE0B60,
    VEHICLE_COGNOSCENTI2 = 0xDBF2D57A,
    VEHICLE_EMPEROR = 0xD7278283,
    VEHICLE_EMPEROR2 = 0x8FC3AADC,
    VEHICLE_EMPEROR3 = 0xB5FCF74E,
    VEHICLE_FUGITIVE = 0x71CB2FFB,
    VEHICLE_GLENDALE = 0x47A6BC1,
    VEHICLE_INGOT = 0xB3206692,
    VEHICLE_INTRUDER = 0x34DD8AA1,
    VEHICLE_LIMO2 = 0xF92AEC4D,
    VEHICLE_PREMIER = 0x8FB66F9B,
    VEHICLE_PRIMO = 0xBB6B404F,
    VEHICLE_PRIMO2 = 0x86618EDA,
    VEHICLE_REGINA = 0xFF22D208,
    VEHICLE_ROMERO = 0x2560B2FC,
    VEHICLE_SCHAFTER2 = 0xB52B5113,
    VEHICLE_SCHAFTER5 = 0xCB0E7CD9,
    VEHICLE_SCHAFTER6 = 0x72934BE4,
    VEHICLE_STAFFORD = 0x1324E960,
    VEHICLE_STANIER = 0xA7EDE74D,
    VEHICLE_STRATUM = 0x66B4FC45,
    VEHICLE_STRETCH = 0x8B13F083,
    VEHICLE_SUPERD = 0x42F2ED16,
    VEHICLE_SURGE = 0x8F0E3594,
    VEHICLE_TAILGATER = 0xC3DDFDCE,
    VEHICLE_WARRENER = 0x51D83328,
    VEHICLE_WASHINGTON = 0x69F06B57
};

enum Service
{
    VEHICLE_AIRBUS = 0x4C80EB0E,
    VEHICLE_BRICKADE = 0xEDC6F847,
    VEHICLE_BUS = 0xD577C962,
    VEHICLE_COACH = 0x84718D34,
    VEHICLE_PBUS2 = 0x149BD32A,
    VEHICLE_RALLYTRUCK = 0x829A3C44,
    VEHICLE_RENTALBUS = 0xBE819C63,
    VEHICLE_TAXI = 0xC703DB5F,
    VEHICLE_TOURBUS = 0x73B1C3CB,
    VEHICLE_TRASH = 0x72435A19,
    VEHICLE_TRASH2 = 0xB527915C,
    VEHICLE_WASTELANDER = 0x8E08EC82
};

enum Sports
{
    VEHICLE_ALPHA = 0x2DB8D1AA,
    VEHICLE_BANSHEE = 0xC1E908D2,
    VEHICLE_BESTIAGTS = 0x4BFCF28B,
    VEHICLE_BLISTA2 = 0x3DEE5EDA,
    VEHICLE_BLISTA3 = 0xDCBC1C3B,
    VEHICLE_BUFFALO = 0xEDD516C6,
    VEHICLE_BUFFALO2 = 0x2BEC3CBE,
    VEHICLE_BUFFALO3 = 0xE2C013E,
    VEHICLE_CARBONIZZARE = 0x7B8AB45F,
    VEHICLE_COMET2 = 0xC1AE4D16,
    VEHICLE_COMET3 = 0x877358AD,
    VEHICLE_COMET4 = 0x5D1903F9,
    VEHICLE_COMET5 = 0x276D98A3,
    VEHICLE_COQUETTE = 0x67BC037,
    VEHICLE_ELEGY = 0xBBA2261,
    VEHICLE_ELEGY2 = 0xDE3D9D22,
    VEHICLE_FELTZER2 = 0x8911B9F5,
    VEHICLE_FLASHGT = 0xB4F32118,
    VEHICLE_FUROREGT = 0xBF1691E0,
    VEHICLE_FUSILADE = 0x1DC0BA53,
    VEHICLE_FUTO = 0x7836CE2F,
    VEHICLE_GB200 = 0x71CBEA98,
    VEHICLE_HOTRING = 0x42836BE5,
    VEHICLE_JESTER = 0xB2A716A3,
    VEHICLE_JESTER2 = 0xBE0E6126,
    VEHICLE_JESTER3 = 0xF330CB6A,
    VEHICLE_KHAMELION = 0x206D1B68,
    VEHICLE_KURUMA = 0xAE2BFE94,
    VEHICLE_KURUMA2 = 0x187D938D,
    VEHICLE_LYNX2 = 0x5DCA7C9A,
    VEHICLE_MASSACRO = 0xF77ADE32,
    VEHICLE_MASSACRO2 = 0xDA5819A3,
    VEHICLE_NEON = 0x91CA96EE,
    VEHICLE_NINEF = 0x3D8FA25C,
    VEHICLE_NINEF2 = 0xA8E38B01,
    VEHICLE_OMNIS = 0xD1AD4937,
    VEHICLE_PARIAH = 0x33B98FE2,
    VEHICLE_PENUMBRA = 0xE9805550,
    VEHICLE_RAIDEN = 0xA4D99B7D,
    VEHICLE_RAPIDGT = 0x8CB29A14,
    VEHICLE_RAPIDGT2 = 0x679450AF,
    VEHICLE_RAPTOR = 0xD7C56D39,
    VEHICLE_REVOLTER = 0xE78CC3D9,
    VEHICLE_RUSTON = 0x2AE524A8,
    VEHICLE_SCHAFTER3 = 0xA774B5A6,
    VEHICLE_SCHAFTER4 = 0x58CF185C,
    VEHICLE_SCHWARZER = 0xD37B7976,
    VEHICLE_SENTINEL3 = 0x41D149AA,
    VEHICLE_SEVEN70 = 0x97398A4B,
    VEHICLE_SPECTER = 0x706E2B40,
    VEHICLE_SPECTER2 = 0x400F5147,
    VEHICLE_STREITER = 0x67D2B389,
    VEHICLE_SULTAN = 0x39DA2754,
    VEHICLE_SURANO = 0x16E478C1,
    VEHICLE_TAMPA2 = 0xC0240885,
    VEHICLE_TROPOS = 0x707E63A4,
    VEHICLE_VERLIERER2 = 0x41B77FA4
};

enum SportsClassic
{
    VEHICLE_ARDENT = 0x97E5533,
    VEHICLE_BTYPE = 0x6FF6914,
    VEHICLE_BTYPE2 = 0xCE6B35A4,
    VEHICLE_BTYPE3 = 0xDC19D101,
    VEHICLE_CASCO = 0x3822BDFE,
    VEHICLE_CHEETAH2 = 0xD4E5F4D,
    VEHICLE_COQUETTE2 = 0x3C4E2113,
    VEHICLE_DELUXO = 0x586765FB,
    VEHICLE_FAGALOA = 0x6068AD86,
    VEHICLE_FELTZER3 = 0xA29D6D10,
    VEHICLE_GT500 = 0x8408F33A,
    VEHICLE_HUSTLER = 0x23CA25F2,
    VEHICLE_INFERNUS2 = 0xAC33179C,
    VEHICLE_JB700 = 0x3EAB5555,
    VEHICLE_MAMBA = 0x9CFFFC56,
    VEHICLE_MANANA = 0x81634188,
    VEHICLE_MICHELLI = 0x3E5BD8D9,
    VEHICLE_MONROE = 0xE62B361B,
    VEHICLE_PEYOTE = 0x6D19CCBC,
    VEHICLE_PIGALLE = 0x404B6381,
    VEHICLE_RAPIDGT3 = 0x7A2EF5E4,
    VEHICLE_RETINUE = 0x6DBD6C0A,
    VEHICLE_SAVESTRA = 0x35DED0DD,
    VEHICLE_STINGER = 0x5C23AF9B,
    VEHICLE_STINGERGT = 0x82E499FA,
    VEHICLE_STROMBERG = 0x34DBA661,
    VEHICLE_SWINGER = 0x1DD4C0FF,
    VEHICLE_TORERO = 0x59A9E570,
    VEHICLE_TORNADO = 0x1BB290BC,
    VEHICLE_TORNADO2 = 0x5B42A5C4,
    VEHICLE_TORNADO3 = 0x690A4153,
    VEHICLE_TORNADO4 = 0x86CF7CDD,
    VEHICLE_TORNADO5 = 0x94DA98EF,
    VEHICLE_TORNADO6 = 0xA31CB573,
    VEHICLE_TURISMO2 = 0xC575DF11,
    VEHICLE_VISERIS = 0xE8A8BA94,
    VEHICLE_Z190 = 0x3201DD49,
    VEHICLE_ZTYPE = 0x2D3BD401
};

enum Super
{
    VEHICLE_ADDER = 0xB779A091,
    VEHICLE_AUTARCH = 0xED552C74,
    VEHICLE_BANSHEE2 = 0x25C5AF13,
    VEHICLE_BULLET = 0x9AE6DDA1,
    VEHICLE_CHEETAH = 0xB1D95DA0,
    VEHICLE_CYCLONE = 0x52FF9437,
    VEHICLE_ENTITYXF = 0xB2FE5CF9,
    VEHICLE_ENTITY2 = 0x8198AEDC,
    VEHICLE_FMJ = 0x5502626C,
    VEHICLE_GP1 = 0x4992196C,
    VEHICLE_INFERNUS = 0x18F25AC7,
    VEHICLE_ITALIGTB = 0x85E8E76B,
    VEHICLE_ITALIGTB2 = 0xE33A477B,
    VEHICLE_LE7B = 0xB6846A55,
    VEHICLE_NERO = 0x3DA47243,
    VEHICLE_NERO2 = 0x4131F378,
    VEHICLE_OSIRIS = 0x767164D6,
    VEHICLE_PENETRATOR = 0x9734F3EA,
    VEHICLE_PFISTER811 = 0x92EF6E04,
    VEHICLE_PROTOTIPO = 0x7E8F677F,
    VEHICLE_REAPER = 0xDF381E5,
    VEHICLE_SC1 = 0x5097F589,
    VEHICLE_SCRAMJET = 0xD9F0503D,
    VEHICLE_SHEAVA = 0x30D3F6D8,
    VEHICLE_SULTANRS = 0xEE6024BC,
    VEHICLE_T20 = 0x6322B39A,
    VEHICLE_TAIPAN = 0xBC5DC07E,
    VEHICLE_TEMPESTA = 0x1044926F,
    VEHICLE_TEZERACT = 0x3D7C6410,
    VEHICLE_TURISMOR = 0x185484E1,
    VEHICLE_TYRANT = 0xE99011C2,
    VEHICLE_TYRUS = 0x7B406EFB,
    VEHICLE_VACCA = 0x142E0DC3,
    VEHICLE_VAGNER = 0x7397224C,
    VEHICLE_VIGILANTE = 0xB5EF4C33,
    VEHICLE_VISIONE = 0xC4810400,
    VEHICLE_VOLTIC = 0x9F4B77BE,
    VEHICLE_VOLTIC2 = 0x3AF76F4A,
    VEHICLE_XA21 = 0x36B4A8A9,
    VEHICLE_ZENTORNO = 0xAC5DF515
};

enum Trailers
{
    VEHICLE_ARMYTANKER = 0xB8081009,
    VEHICLE_ARMYTRAILER2 = 0x9E6B14D6,
    VEHICLE_BALETRAILER = 0xE82AE656,
    VEHICLE_BOATTRAILER = 0x1F3D44B5,
    VEHICLE_CABLECAR = 0xC6C3242D,
    VEHICLE_DOCKTRAILER = 0x806EFBEE,
    VEHICLE_GRAINTRAILER = 0x3CC7F596,
    VEHICLE_PROPTRAILER = 0x153E1B0A,
    VEHICLE_RAKETRAILER = 0x174CB172,
    VEHICLE_TR2 = 0x7BE032C6,
    VEHICLE_TR3 = 0x6A59902D,
    VEHICLE_TR4 = 0x7CAB34D0,
    VEHICLE_TRFLAT = 0xAF62F6B2,
    VEHICLE_TVTRAILER = 0x967620BE,
    VEHICLE_TANKER = 0xD46F4737,
    VEHICLE_TANKER2 = 0x74998082,
    VEHICLE_TRAILERLOGS = 0x782A236D,
    VEHICLE_TRAILERSMALL = 0x2A72BEAB,
    VEHICLE_TRAILERS = 0xCBB2BE0E,
    VEHICLE_TRAILERS2 = 0xA1DA3C91,
    VEHICLE_TRAILERS3 = 0x8548036D
};

enum Trains
{
    VEHICLE_FREIGHT = 0x3D6AAA9B,
    VEHICLE_FREIGHTCAR = 0x0AFD22A6,
    VEHICLE_FREIGHTCONT1 = 0x36DCFF98,
    VEHICLE_FREIGHTCONT2 = 0x0E512E79,
    VEHICLE_FREIGHTGRAIN = 0x264D9262,
    VEHICLE_TANKERCAR = 0x22EDDC30
};

enum Utility
{
    VEHICLE_AIRTUG = 0x5D0AAC8F,
    VEHICLE_CADDY = 0x44623884,
    VEHICLE_CADDY2 = 0xDFF0594C,
    VEHICLE_CADDY3 = 0xD227BDBB,
    VEHICLE_CARACARA = 0x4ABEBF23,
    VEHICLE_DOCKTUG = 0xCB44B1CA,
    VEHICLE_FORKLIFT = 0x58E49664,
    VEHICLE_MOWER = 0x6A4BD8F6,
    VEHICLE_RIPLEY = 0xCD935EF9,
    VEHICLE_SADLER = 0xDC434E51,
    VEHICLE_SADLER2 = 0x2BC345D1,
    VEHICLE_SCRAP = 0x9A9FD3DF,
    VEHICLE_TOWTRUCK = 0xB12314E0,
    VEHICLE_TOWTRUCK2 = 0xE5A2D6C6,
    VEHICLE_TRACTOR = 0x61D6BA8C,
    VEHICLE_TRACTOR2 = 0x843B73DE,
    VEHICLE_TRACTOR3 = 0x562A97BD,
    VEHICLE_UTILLITRUCK = 0x1ED0A534,
    VEHICLE_UTILLITRUCK2 = 0x34E6BF6B,
    VEHICLE_UTILLITRUCK3 = 0x7F2153DF
};

enum Vans
{
    VEHICLE_BISON = 0xFEFD644F,
    VEHICLE_BISON2 = 0x7B8297C5,
    VEHICLE_BISON3 = 0x67B3F020,
    VEHICLE_BOBCATXL = 0x3FC5D440,
    VEHICLE_BOXVILLE = 0x898ECCEA,
    VEHICLE_BOXVILLE2 = 0xF21B33BE,
    VEHICLE_BOXVILLE3 = 0x07405E08,
    VEHICLE_BOXVILLE4 = 0x1A79847A,
    VEHICLE_BOXVILLE5 = 0x28AD20E1,
    VEHICLE_BURRITO = 0xAFBB2CA4,
    VEHICLE_BURRITO2 = 0xC9E8FF76,
    VEHICLE_BURRITO3 = 0x98171BD3,
    VEHICLE_BURRITO4 = 0x353B561D,
    VEHICLE_BURRITO5 = 0x437CF2A0,
    VEHICLE_CAMPER = 0x6FD95F68,
    VEHICLE_GBURRITO = 0x97FA4F36,
    VEHICLE_GBURRITO2 = 0x11AA0E14,
    VEHICLE_JOURNEY = 0xF8D48E7A,
    VEHICLE_MINIVAN = 0xED7EADA4,
    VEHICLE_MINIVAN2 = 0xBCDE91F0,
    VEHICLE_PARADISE = 0x58B3979C,
    VEHICLE_PONY = 0xF8DE29A8,
    VEHICLE_PONY2 = 0x38408341,
    VEHICLE_RUMPO = 0x4543B74D,
    VEHICLE_RUMPO2 = 0x961AFEF7,
    VEHICLE_RUMPO3 = 0x57F682AF,
    VEHICLE_SPEEDO = 0xCFB3870C,
    VEHICLE_SPEEDO2 = 0x2B6DC64A,
    VEHICLE_SPEEDO4 = 0xD17099D,
    VEHICLE_SURFER = 0x29B0DA97,
    VEHICLE_SURFER2 = 0xB1D80E06,
    VEHICLE_TACO = 0x744CA80D,
    VEHICLE_YOUGA = 0x03E5F6B8,
    VEHICLE_YOUGA2 = 0x3D29CD2B
};

enum ArenaWar
{
    VEHICLE_BRUISER = 0x27D79225,
    VEHICLE_BRUISER2 = 0x9B065C9E,
    VEHICLE_BRUISER3 = 0x8644331A,
    VEHICLE_BRUTUS = 0x7F81A829,
    VEHICLE_BRUTUS2 = 0x8F49AE28,
    VEHICLE_BRUTUS3 = 0x798682A2,
    VEHICLE_CERBERUS = 0xD039510B,
    VEHICLE_CERBERUS2 = 0x287FA449,
    VEHICLE_CERBERUS3 = 0x71D3B6F0,
    VEHICLE_CLIQUE = 0xA29F78B0,
    VEHICLE_DEATHBIKE = 0xFE5F0722,
    VEHICLE_DEATHBIKE2 = 0x93F09558,
    VEHICLE_DEATHBIKE3 = 0xAE12C99C,
    VEHICLE_DEVESTE = 0x5EE005DA,
    VEHICLE_DEVIANT = 0x4C3FFF49,
    VEHICLE_DOMINATOR4 = 0xD6FB0F30,
    VEHICLE_DOMINATOR5 = 0xAE0A3D4F,
    VEHICLE_DOMINATOR6 = 0xB2E046FB,
    VEHICLE_IMPALER = 0x83070B62,
    VEHICLE_IMPALER2 = 0x3C26BD0C,
    VEHICLE_IMPALER3 = 0x8D45DF49,
    VEHICLE_IMPALER4 = 0x9804F4C7,
    VEHICLE_IMPERATOR = 0x1A861243,
    VEHICLE_IMPERATOR2 = 0x619C1B82,
    VEHICLE_IMPERATOR3 = 0xD2F77E37,
    VEHICLE_ISSI4 = 0x256E92BA,
    VEHICLE_ISSI5 = 0x5BA0FF1E,
    VEHICLE_ISSI6 = 0x49E25BA1,
    VEHICLE_ITALIGTO = 0xEC3E3404,
    VEHICLE_MONSTER3 = 0x669EB40A,
    VEHICLE_MONSTER4 = 0x32174AFC,
    VEHICLE_MONSTER5 = 0xD556917C,
    VEHICLE_RCBANDITO = 0xEEF345EC,
    VEHICLE_SCARAB2 = 0x5BEB3CE0,
    VEHICLE_SCARAB3 = 0xDD71BFEB,
    VEHICLE_SCHLAGEN = 0xE1C03AB0,
    VEHICLE_SLAMVAN4 = 0x8526E2F5,
    VEHICLE_SLAMVAN5 = 0x163F8520,
    VEHICLE_SLAMVAN6 = 0x67D52852,
    VEHICLE_TOROS = 0xBA5334AC,
    VEHICLE_TULIP = 0x56D42971,
    VEHICLE_VAMOS = 0xFD128DFD,
    VEHICLE_ZR380 = 0x20314B42,
    VEHICLE_ZR3802 = 0xBE11EFC6,
    VEHICLE_ZR3803 = 0xA7DCC35C
};