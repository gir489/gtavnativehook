#pragma once

namespace ENGINE
{
    bool STAT_SET_INT(Hash statName, int value, BOOL save);
    bool STAT_SET_FLOAT(Hash statName, float value, BOOL save);
    bool STAT_SET_BOOL(Hash statName, bool value, BOOL save);
    void TRIGGER_SCRIPT_EVENT(int eventGroup, uint64_t* args, int argCount, int bitset);
    __int64* GET_GLOBAL_PTR(int index);
    Vehicle CREATE_VEHICLE(Hash modelHash, float x, float y, float z, float heading, BOOL isNetwork, BOOL thisScriptCheck);
}

namespace VEHICLE
{
    static Void SET_VEHICLE_INTERIOR_COLOUR(Vehicle vehicle, int color) { return NativeInvoke::Invoke<Void, Vehicle, int>(0xAA16E7FC8833903E, vehicle, color); } // F40DD601A65F7F19}
    static Void GET_VEHICLE_INTERIOR_COLOUR(Vehicle vehicle, int* color) { return NativeInvoke::Invoke<Void, Vehicle, int*>(0xC7C5DC591E937951, vehicle, color); } // 7D1464D472D32136
    static Void SET_VEHICLE_DASHBOARD_COLOUR(Vehicle vehicle, int color) { return NativeInvoke::Invoke<Void, Vehicle, int>(0xB6A2BF3D4E98B30C, vehicle, color); } // 6089CDF6A57F326C
    static Void GET_VEHICLE_DASHBOARD_COLOUR(Vehicle vehicle, int* color) { return NativeInvoke::Invoke<Void, Vehicle, int*>(0xDB939F97A405AB34, vehicle, color); } // B7635E80A5C31BFF 
}

namespace SYSTEM
{
    static int START_NEW_SCRIPT(char* scriptName, int stackSize) { return NativeInvoke::Invoke<int, char*, int>(0xE81651AD79516E48, scriptName, stackSize); } // 0xE81651AD79516E48 0x3F166D0E
    static int START_NEW_SCRIPT_WITH_ARGS(char* scriptName, Any* args, int argCount, int stackSize) { return NativeInvoke::Invoke<int, char*, Any*, int, int>(0xB8BA7F44DF1575E1, scriptName, args, argCount, stackSize); } // 0xB8BA7F44DF1575E1 0x4A2100E4
    static int _START_NEW_STREAMED_SCRIPT(Hash scriptHash, int stackSize) { return NativeInvoke::Invoke<int, Hash, int>(0xEB1C67C3A5333A92, scriptHash, stackSize); } // 0xEB1C67C3A5333A92 0x8D15BE5D
    static int _START_NEW_STREAMED_SCRIPT_WITH_ARGS(Hash scriptHash, Any* args, int argCount, int stackSize) { return NativeInvoke::Invoke<int, Hash, Any*>(0xC4BB298BD441BE78, scriptHash, args, argCount, stackSize); } // 0xC4BB298BD441BE78 0xE38A3AD4
    static int TIMERA() { return NativeInvoke::Invoke<int>(0x83666F9FB8FEBD4B); } // 0x83666F9FB8FEBD4B 0x45C8C188
    static int TIMERB() { return NativeInvoke::Invoke<int>(0xC9D9444186B5A374); } // 0xC9D9444186B5A374 0x330A9C0C
    static Void SETTIMERA(int value) { return NativeInvoke::Invoke<Void, int>(0xC1B1E9A034A63A62, value); } // 0xC1B1E9A034A63A62 0x35785333
    static Void SETTIMERB(int value) { return NativeInvoke::Invoke<Void, int>(0x5AE11BC36633DE4E, value); } // 0x5AE11BC36633DE4E 0x27C1B7C6
    static float TIMESTEP() { return NativeInvoke::Invoke<float>(0x0000000050597EE2); } // 0x0000000050597EE2 0x50597EE2
    static float SIN(float value) { return NativeInvoke::Invoke<float, float>(0x0BADBFA3B172435F, value); } // 0x0BADBFA3B172435F 0xBF987F58
    static float COS(float value) { return NativeInvoke::Invoke<float, float>(0xD0FFB162F40A139C, value); } // 0xD0FFB162F40A139C 0x00238FE9
    static float SQRT(float value) { return NativeInvoke::Invoke<float, float>(0x71D93B57D07F9804, value); } // 0x71D93B57D07F9804 0x145C7701
    static float POW(float base, float exponent) { return NativeInvoke::Invoke<float, float, float>(0xE3621CC40F31FE2E, base, exponent); } // 0xE3621CC40F31FE2E 0x85D134F8
    static float VMAG(float p0, float p1, float p2) { return NativeInvoke::Invoke<float, float, float, float>(0x652D2EEEF1D3E62C, p0, p1, p2); } // 0x652D2EEEF1D3E62C 0x1FCF1ECD
    static float VMAG2(float p0, float p1, float p2) { return NativeInvoke::Invoke<float, float, float, float>(0xA8CEACB4F35AE058, p0, p1, p2); } // 0xA8CEACB4F35AE058 0xE796E629
    static float VDIST(float x1, float y1, float z1, float x2, float y2, float z2) { return NativeInvoke::Invoke<float, float, float, float, float, float, float>(0x2A488C176D52CCA5, x1, y1, z1, x2, y2, z2); } // 0x2A488C176D52CCA5 0x3C08ECB7
    static float VDIST2(float x1, float y1, float z1, float x2, float y2, float z2) { return NativeInvoke::Invoke<float, float, float, float, float, float, float>(0xB7A628320EFF8E47, x1, y1, z1, x2, y2, z2); } // 0xB7A628320EFF8E47 0xC85DEF1F
    static int SHIFT_LEFT(int value, int bitShift) { return NativeInvoke::Invoke<int, int, int>(0xEDD95A39E5544DE8, value, bitShift); } // 0xEDD95A39E5544DE8 0x314CC6CD
    static int SHIFT_RIGHT(int value, int bitShift) { return NativeInvoke::Invoke<int, int, int>(0x97EF1E5BCE9DC075, value, bitShift); } // 0x97EF1E5BCE9DC075 0x352633CA
    static int FLOOR(float value) { return NativeInvoke::Invoke<int, float>(0xF34EE736CF047844, value); } // 0xF34EE736CF047844 0x32E9BE04
    static int CEIL(float value) { return NativeInvoke::Invoke<int, float>(0x11E019C8F43ACC8A, value); } // 0x11E019C8F43ACC8A 0xD536A1DF
    static int ROUND(float value) { return NativeInvoke::Invoke<int, float>(0xF2DB717A73826179, value); } // 0xF2DB717A73826179 0x323B0E24
    static float TO_FLOAT(int value) { return NativeInvoke::Invoke<float, int>(0xBBDA792448DB5A89, value); } // 0xBBDA792448DB5A89 0x67116627
}