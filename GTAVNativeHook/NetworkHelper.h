#pragma once

//Function to determine if a Player object is on your Rockstar Social Club friends list.
static BOOL IsPlayerFriend(Player player)
{
    int handle[26];
    NETWORK::NETWORK_HANDLE_FROM_PLAYER(player, &handle[0], 13);
    return NETWORK::NETWORK_IS_HANDLE_VALID(&handle[0], 13) && NETWORK::NETWORK_IS_FRIEND(&handle[0]);
}

static int GetNetworkHandle(Player player)
{
    int handle[26];
    NETWORK::NETWORK_HANDLE_FROM_PLAYER(player, &handle[0], 13);
    if (NETWORK::NETWORK_IS_HANDLE_VALID(&handle[0], 13))
        return handle[0];
    return NULL;
}

static Player GetPlayerFromSCID(int scid)
{
    string memberHandle = std::to_string(scid);
    int handle[26];
    NETWORK::NETWORK_HANDLE_FROM_MEMBER_ID((char*)memberHandle.c_str(), &handle[0], 13);
    if (NETWORK::NETWORK_IS_HANDLE_VALID(&handle[0], 13))
    {
        Player player = NETWORK::NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(&handle[0]);
        if (player)
            return player;
    }
    return -1;
}

static char* GetNameFromSCID(int scid)
{
    Player messenger = GetPlayerFromSCID(scid);
    if (messenger == -1)
        return XorStr("UNKNOWN_NAME");
    return NETWORK::NETWORK_PLAYER_GET_NAME(messenger);
}