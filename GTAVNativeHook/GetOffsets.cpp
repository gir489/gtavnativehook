#include "dllmain.h"


void COffsets::Init()
{
    //bool isSteam = (((DWORD64)gOffsets.GetModuleBaseAddress(gOffsets.GetWC(XorStr("steam_api64.dll")))) != NULL);

    IsDlcPresent = FindPattern::Find(XorStr("48 89 5C 24 ? 57 48 83 EC 20 81 F9")); //Good since 350.
    pLog->WriteToFile(XorStr("IsDlcPresent: GTA5.exe+%X"), IsDlcPresent - dwImageBase);

    dwGetModelTableFunctionAddress = FindPattern::Find(XorStr("48 8B C8 FF 52 30 84 C0 74 05 48")); //Good since 350.
    pLog->WriteToFile(XorStr("dwGetModelTableFunctionAddress: GTA5.exe+%X"), dwGetModelTableFunctionAddress - dwImageBase);

    DWORD64 dwRegistrationTable = FindPattern::Find(XorStr("48 8D 0D ? ? ? ? 48 8B 14 FA")); //Good since 1493.1.
    gOffsets.RegistrationTable = dwRegistrationTable + *(DWORD*)(dwRegistrationTable + 3) + 7;
    pLog->WriteToFile(XorStr("dwRegistrationTable: GTA5.exe+%X"), dwRegistrationTable - dwImageBase);

    dwAddOwnedExplosionBypass = FindPattern::Find(XorStr("0F 85 ? ? ? ? 48 8B 05 ? ? ? ? 48 8B 48 08 E8")); //Good since 1493.1
    pLog->WriteToFile(XorStr("dwAddOwnedExplosionBypass: GTA5.exe+%X"), dwAddOwnedExplosionBypass - dwImageBase);

    DWORD64 replayInterfaceAddress = FindPattern::Find(XorStr("48 8D 3D ? ? ? ? 75 16")); //Good since 350.
    gOffsets.replayInterface = *(CReplayInterface**)(replayInterfaceAddress + *(DWORD*)(replayInterfaceAddress + 3) + 7);
    pLog->WriteToFile(XorStr("replayInterface: GTA5.exe+%X"), (DWORD64)replayInterface - dwImageBase);

    gOffsets.ModelCrashLocation = FindPattern::Find(XorStr("4C 8B 42 28 65 48 8B 0C 25"));
    pLog->WriteToFile(XorStr("ModelCrashLocation: GTA5.exe+%X"), (DWORD64)ModelCrashLocation - dwImageBase);

    gOffsets.ptrToHandleAddress = FindPattern::Find(XorStr("48 89 5C 24 ? 48 89 74 24 ? 57 48 83 EC 20 8B 15 ? ? ? ? 48 8B F9 48 83 C1 10 33 DB")); //Good since 350.
    pLog->WriteToFile(XorStr("ptrToHandleAddress: GTA5.exe+%X"), ptrToHandleAddress - dwImageBase);
    
    gOffsets.ClearPedTasksEvent = FindPattern::Find(XorStr("48 89 5C 24 ? 48 89 74 24 ? 57 48 83 EC 30 0F B7 51 30")); //Good since 350.
    pLog->WriteToFile(XorStr("ClearPedTasksEvent: GTA5.exe+%X"), ClearPedTasksEvent - dwImageBase);
    
    gOffsets.ReportFunction = FindPattern::Find(XorStr("48 89 5C 24 ? 48 89 74 24 ? 55 57 41 55 41 56 41 57 48 8B EC 48 83 EC 60 8B 79 30")); //Good since 350.
    pLog->WriteToFile(XorStr("ReportFunction: GTA5.exe+%X"), ReportFunction - dwImageBase);

    gOffsets.TextMessageFunction = FindPattern::Find(XorStr("48 89 5C 24 ? 48 89 74 24 ? 57 48 81 EC ? ? ? ? 48 8B D9 E8")); //Good since 350.
    pLog->WriteToFile(XorStr("TextMessageFunction: GTA5.exe+%X"), TextMessageFunction - dwImageBase);

    gOffsets.GetEventData = FindPattern::Find(XorStr("48 89 5C 24 ? 57 48 83 EC 20 49 8B F8 4C 8D 05")); //Good since 350.
    pLog->WriteToFile(XorStr("GetEventData: GTA5.exe+%X"), GetEventData - dwImageBase);

    DWORD64 blipCollectionSignature = FindPattern::Find(XorStr("4C 8D 05 ? ? ? ? 0F B7 C1")); //Good since 350.
    pBlipList = (BlipList*)(blipCollectionSignature + *(PDWORD)(blipCollectionSignature + 3) + 7);
    pLog->WriteToFile(XorStr("pBlipList: GTA5.exe+%X"), (DWORD64)pBlipList - dwImageBase);

    DWORD64 globalPointerSignature = FindPattern::Find(XorStr("4C 8D 05 ? ? ? ? 4D 8B 08 4D 85 C9 74 11")); //Good since 350.
    gOffsets.GlobalBase = (__int64**)(globalPointerSignature + *(PDWORD)(globalPointerSignature + 3) + 7);
    pLog->WriteToFile(XorStr("GlobalBase: GTA5.exe+%X"), (DWORD64)GlobalBase - dwImageBase);

    //STATS
    STAT_SET_INT = FindPattern::Find(XorStr("48 89 5C 24 ? 48 89 74 24 ? 89 54 24 10 55 57 41 57")); //Good since 350.
    pLog->WriteToFile(XorStr("STAT_SET_INT: GTA5.exe+%X"), STAT_SET_INT - dwImageBase);

    STAT_SET_FLOAT = FindPattern::Find(XorStr("48 89 5C 24 ? F3 0F 11 4C 24 ? 57 48 83 EC 40")); //Good since 350.
    pLog->WriteToFile(XorStr("STAT_SET_FLOAT: GTA5.exe+%X"), STAT_SET_FLOAT - dwImageBase);

    STAT_SET_BOOL = FindPattern::Find(XorStr("48 89 5C 24 ? 88 54 24 10 57 48 83 EC 40")); //Good since 350.
    pLog->WriteToFile(XorStr("STAT_SET_BOOL: GTA5.exe+%X"), STAT_SET_BOOL - dwImageBase);

    //SCRIPT
    TRIGGER_SCRIPT_EVENT = FindPattern::Find(XorStr("48 8B C4 48 89 58 08 48 89 68 10 48 89 70 18 48 89 78 20 41 56 48 81 EC ? ? ? ? 45 8B F0 41 8B F9")); //Good since 350. 
    pLog->WriteToFile(XorStr("TRIGGER_SCRIPT_EVENT: GTA5.exe+%X"), TRIGGER_SCRIPT_EVENT - dwImageBase);

    HOOKS::Init();
}

HMODULE WINAPI COffsets::GetModuleBaseAddress(LPCWSTR moduleName)
{
    PEB *pPeb = NULL;
    LIST_ENTRY *pListEntry = NULL;
    LDR_DATA_TABLE_ENTRY *pLdrDataTableEntry = NULL;

    pPeb = (PPEB)__readgsqword(0x60);

    if (pPeb == NULL)
        return NULL;

    pLdrDataTableEntry = (PLDR_DATA_TABLE_ENTRY)pPeb->Ldr->InMemoryOrderModuleList.Flink;
    pListEntry = pPeb->Ldr->InMemoryOrderModuleList.Flink;

    do
    {
        if (lstrcmpiW(pLdrDataTableEntry->FullDllName.Buffer, moduleName) == 0)
            return (HMODULE)pLdrDataTableEntry->Reserved2[0];

        pListEntry = pListEntry->Flink;
        pLdrDataTableEntry = (PLDR_DATA_TABLE_ENTRY)(pListEntry->Flink);

    } while (pListEntry != pPeb->Ldr->InMemoryOrderModuleList.Flink);

    return NULL;
}

PIMAGE_NT_HEADERS COffsets::GetNTHeader(HMODULE hmModule)
{
    if (hmModule == nullptr)
        return nullptr;

    IMAGE_DOS_HEADER* pDosHeader = reinterpret_cast<IMAGE_DOS_HEADER*>(hmModule);

    if (pDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
        return nullptr;

    PIMAGE_NT_HEADERS pNtHeader = make_ptr< PIMAGE_NT_HEADERS >(hmModule, pDosHeader->e_lfanew);

    if (pNtHeader->Signature != IMAGE_NT_SIGNATURE)
        return nullptr;

    return pNtHeader;
}

const wchar_t *COffsets::GetWC(const char *c)
{
    size_t size = strlen(c) + 1;
    size_t outSize;
    wchar_t* wideChar = new wchar_t[size];
    mbstowcs_s(&outSize, wideChar, size, c, size - 1);
    
    return wideChar;
}