#pragma once

static void AddClanLogoToVehicle(Vehicle vehicle, Ped ped)
{
    vector3_t x, y, z;
    float scale;
    Hash modelHash = ENTITY::GET_ENTITY_MODEL(vehicle);
    if (GetVehicleInfoForClanLogo(modelHash, x, y, z, scale))
    {
        int alpha = 200;
        if (modelHash == VEHICLE_WINDSOR || modelHash == VEHICLE_COMET4)
            alpha = 255;

        GRAPHICS::_ADD_CLAN_DECAL_TO_VEHICLE(vehicle, ped, ENTITY::GET_ENTITY_BONE_INDEX_BY_NAME(vehicle, XorStr("chassis_dummy")), x.x, x.y, x.z, y.x, y.y, y.z, z.x, z.y, z.z, scale, 0, alpha);
        if (y.z >= 0.0f)
            GRAPHICS::_ADD_CLAN_DECAL_TO_VEHICLE(vehicle, ped, ENTITY::GET_ENTITY_BONE_INDEX_BY_NAME(vehicle, XorStr("chassis_dummy")), x.x * -1.0f, x.y, x.z, y.x * -1, y.y, y.z, z.x * -1.0f, z.y * -1.0f, z.z, scale, 1, alpha);
    }
}

static void doMPStuffToSpawnedCar(Vehicle vehicle, Player player)
{
    NETWORK::NETWORK_REQUEST_CONTROL_OF_ENTITY(vehicle);
    DECORATOR::DECOR_SET_INT(vehicle, XorStr("MPBitset"), 0);
    ENTITY::_SET_ENTITY_SOMETHING(vehicle, TRUE);
    auto networkId = NETWORK::VEH_TO_NET(vehicle);
    if (NETWORK::NETWORK_GET_ENTITY_IS_NETWORKED(vehicle))
        NETWORK::SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(networkId, true);
    ENTITY::SET_ENTITY_AS_MISSION_ENTITY(vehicle, FALSE, 1);
}

static Vehicle ClonePedVehicle(Ped ped)
{
    Vehicle pedVeh = NULL;
    Ped playerPed = PLAYER::PLAYER_PED_ID();
    if (PED::IS_PED_IN_ANY_VEHICLE(ped, FALSE))
        pedVeh = PED::GET_VEHICLE_PED_IS_IN(ped, FALSE);
    else //If they're not in a vehicle, try to get their last vehicle.
        pedVeh = PED::GET_VEHICLE_PED_IS_IN(ped, TRUE);
    if (ENTITY::DOES_ENTITY_EXIST(pedVeh))
    {
        Hash vehicleModelHash = ENTITY::GET_ENTITY_MODEL(pedVeh);
        STREAMING::REQUEST_MODEL(vehicleModelHash); //This should already be loaded since we're stealing it from someone in memory.
        Vector3 playerPosition = ENTITY::GET_ENTITY_COORDS(playerPed, FALSE);
        Vehicle playerVeh = ENGINE::CREATE_VEHICLE(vehicleModelHash, playerPosition.x, playerPosition.y, playerPosition.z, ENTITY::GET_ENTITY_HEADING(playerPed), TRUE, FALSE);
        NETWORK::NETWORK_REQUEST_CONTROL_OF_ENTITY(playerVeh);
        doMPStuffToSpawnedCar(playerVeh, PLAYER::PLAYER_ID());
        PED::SET_PED_INTO_VEHICLE(playerPed, playerVeh, SEAT_DRIVER);
        int primaryColor, secondaryColor;
        VEHICLE::GET_VEHICLE_COLOURS(pedVeh, &primaryColor, &secondaryColor);
        VEHICLE::SET_VEHICLE_COLOURS(playerVeh, primaryColor, secondaryColor);
        if (VEHICLE::GET_IS_VEHICLE_PRIMARY_COLOUR_CUSTOM(pedVeh))
        {
            int r, g, b;
            VEHICLE::GET_VEHICLE_CUSTOM_PRIMARY_COLOUR(pedVeh, &r, &g, &b);
            VEHICLE::SET_VEHICLE_CUSTOM_PRIMARY_COLOUR(playerVeh, r, g, b);
        }
        if (VEHICLE::GET_IS_VEHICLE_SECONDARY_COLOUR_CUSTOM(pedVeh))
        {
            int r, g, b;
            VEHICLE::GET_VEHICLE_CUSTOM_SECONDARY_COLOUR(pedVeh, &r, &g, &b);
            VEHICLE::SET_VEHICLE_CUSTOM_SECONDARY_COLOUR(playerVeh, r, g, b);
        }
        if (VEHICLE::IS_THIS_MODEL_A_CAR(vehicleModelHash) || VEHICLE::IS_THIS_MODEL_A_BIKE(vehicleModelHash))
        {
            VEHICLE::SET_VEHICLE_MOD_KIT(playerVeh, 0);
            VEHICLE::SET_VEHICLE_WHEEL_TYPE(playerVeh, VEHICLE::GET_VEHICLE_WHEEL_TYPE(pedVeh));
            for (int i = 0; i <= MOD_LIVERY; i++)
            {
                if (i > MOD_ARMOR && i < MOD_FRONTWHEEL)
                    VEHICLE::TOGGLE_VEHICLE_MOD(playerVeh, i, VEHICLE::IS_TOGGLE_MOD_ON(pedVeh, i));
                else
                    VEHICLE::SET_VEHICLE_MOD(playerVeh, i, VEHICLE::GET_VEHICLE_MOD(pedVeh, i), VEHICLE::GET_VEHICLE_MOD_VARIATION(pedVeh, i));
            }
            int tireSmokeColor[3], pearlescentColor, wheelColor;
            VEHICLE::GET_VEHICLE_TYRE_SMOKE_COLOR(pedVeh, &tireSmokeColor[0], &tireSmokeColor[1], &tireSmokeColor[2]);
            VEHICLE::SET_VEHICLE_TYRE_SMOKE_COLOR(playerVeh, tireSmokeColor[0], tireSmokeColor[1], tireSmokeColor[2]);
            VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(playerVeh, VEHICLE::GET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(pedVeh));
            VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT(playerVeh, VEHICLE::GET_VEHICLE_NUMBER_PLATE_TEXT(pedVeh));
            VEHICLE::GET_VEHICLE_EXTRA_COLOURS(pedVeh, &pearlescentColor, &wheelColor);
            VEHICLE::SET_VEHICLE_EXTRA_COLOURS(playerVeh, pearlescentColor, wheelColor);
            if (VEHICLE::IS_VEHICLE_A_CONVERTIBLE(pedVeh, 0))
            {
                int convertableState = VEHICLE::GET_CONVERTIBLE_ROOF_STATE(pedVeh);
                if (convertableState == 0 || convertableState == 3 || convertableState == 5)
                    VEHICLE::RAISE_CONVERTIBLE_ROOF(playerVeh, 1);
                else
                    VEHICLE::LOWER_CONVERTIBLE_ROOF(playerVeh, 1);
            }
            for (int i = 0; i <= NEON_BACK; i++)
            {
                VEHICLE::_SET_VEHICLE_NEON_LIGHT_ENABLED(playerVeh, i, VEHICLE::_IS_VEHICLE_NEON_LIGHT_ENABLED(pedVeh, i));
            }
            for (int i = 0; i <= 11; i++)
            {
                if (VEHICLE::DOES_EXTRA_EXIST(pedVeh, i))
                    VEHICLE::SET_VEHICLE_EXTRA(playerVeh, i, !VEHICLE::IS_VEHICLE_EXTRA_TURNED_ON(pedVeh, i));
            }
            if ((VEHICLE::GET_VEHICLE_LIVERY_COUNT(pedVeh) > 1) && VEHICLE::GET_VEHICLE_LIVERY(pedVeh) >= 0)
            {
                VEHICLE::SET_VEHICLE_LIVERY(playerVeh, VEHICLE::GET_VEHICLE_LIVERY(pedVeh));
            }
            int neonColor[3];
            VEHICLE::_GET_VEHICLE_NEON_LIGHTS_COLOUR(pedVeh, &neonColor[0], &neonColor[1], &neonColor[2]);
            VEHICLE::_SET_VEHICLE_NEON_LIGHTS_COLOUR(playerVeh, neonColor[0], neonColor[1], neonColor[2]);
            VEHICLE::SET_VEHICLE_WINDOW_TINT(playerVeh, VEHICLE::GET_VEHICLE_WINDOW_TINT(pedVeh));
            VEHICLE::SET_VEHICLE_DIRT_LEVEL(playerVeh, VEHICLE::GET_VEHICLE_DIRT_LEVEL(pedVeh));
            VEHICLE::SET_VEHICLE_ENGINE_ON(playerVeh, TRUE, TRUE, TRUE);
            int interiorColor, dashboardColor;
            VEHICLE::GET_VEHICLE_INTERIOR_COLOUR(pedVeh, &interiorColor);
            VEHICLE::GET_VEHICLE_DASHBOARD_COLOUR(pedVeh, &dashboardColor);
            VEHICLE::SET_VEHICLE_INTERIOR_COLOUR(playerVeh, interiorColor);
            VEHICLE::SET_VEHICLE_DASHBOARD_COLOUR(playerVeh, dashboardColor);
            if (GRAPHICS::_DOES_VEHICLE_HAVE_DECAL(pedVeh, 0) == TRUE)
            {
                AddClanLogoToVehicle(playerVeh, ped);
            }
        }
        STREAMING::SET_MODEL_AS_NO_LONGER_NEEDED(vehicleModelHash);
    }
    return pedVeh;
}

static void DumpVehicleStats(Vehicle vehicle)
{
    if (ENTITY::DOES_ENTITY_EXIST(vehicle) == FALSE || ENTITY::IS_ENTITY_A_VEHICLE(vehicle) == FALSE)
    {
        pLog->WriteToFile(XorStr("***ERROR*** DumpVehicleStats was passed an incorrect entity"));
        return;
    }
    int primaryColor, secondaryColor;
    VEHICLE::GET_VEHICLE_COLOURS(vehicle, &primaryColor, &secondaryColor);
    pLog->WriteToFile(XorStr("vehicleModelHash = VEHICLE_%s; //0x%X"), VEHICLE::GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(ENTITY::GET_ENTITY_MODEL(vehicle)), ENTITY::GET_ENTITY_MODEL(vehicle));
    pLog->WriteToFile(XorStr("VEHICLE::SET_VEHICLE_COLOURS(playerVeh, %u, %u);"), primaryColor, secondaryColor);
    if (VEHICLE::GET_IS_VEHICLE_PRIMARY_COLOUR_CUSTOM(vehicle))
    {
        int r, g, b;
        VEHICLE::GET_VEHICLE_CUSTOM_PRIMARY_COLOUR(vehicle, &r, &g, &b);
        pLog->WriteToFile(XorStr("VEHICLE::SET_VEHICLE_CUSTOM_PRIMARY_COLOUR(playerVeh, %i, %i, %i);"), r, g, b);
    }
    if (VEHICLE::GET_IS_VEHICLE_SECONDARY_COLOUR_CUSTOM(vehicle))
    {
        int r, g, b;
        VEHICLE::GET_VEHICLE_CUSTOM_SECONDARY_COLOUR(vehicle, &r, &g, &b);
        pLog->WriteToFile(XorStr("VEHICLE::SET_VEHICLE_CUSTOM_SECONDARY_COLOUR(playerVeh, %i, %i, %i);"), r, g, b);
    }
    pLog->WriteToFile(XorStr("VEHICLE::SET_VEHICLE_WINDOW_TINT(playerVeh, %i);"), VEHICLE::GET_VEHICLE_WINDOW_TINT(vehicle));
    if (VEHICLE::IS_THIS_MODEL_A_CAR(ENTITY::GET_ENTITY_MODEL(vehicle)) == TRUE || VEHICLE::IS_THIS_MODEL_A_BIKE(ENTITY::GET_ENTITY_MODEL(vehicle)))
    {
        pLog->WriteToFile(XorStr("VEHICLE::SET_VEHICLE_WHEEL_TYPE(playerVeh, %i);"), VEHICLE::GET_VEHICLE_WHEEL_TYPE(vehicle));
        static char* modNames[] = { XorStr("MOD_SPOILERS"), XorStr("MOD_FRONTBUMPER"), XorStr("MOD_REARBUMPER"), XorStr("MOD_SIDESKIRT"), XorStr("MOD_EXHAUST"), XorStr("MOD_FRAME"), XorStr("MOD_GRILLE"), XorStr("MOD_HOOD"), XorStr("MOD_FENDER"), XorStr("MOD_RIGHTFENDER"), XorStr("MOD_ROOF"), XorStr("MOD_ENGINE"), XorStr("MOD_BRAKES"), XorStr("MOD_TRANSMISSION"), XorStr("MOD_HORNS"), XorStr("MOD_SUSPENSION"), XorStr("MOD_ARMOR"), "", XorStr("MOD_TURBO"), "", XorStr("MOD_TIRESMOKE"), "", XorStr("MOD_XENONHEADLIGHTS"), XorStr("MOD_FRONTWHEEL"), XorStr("MOD_REARWHEEL"), XorStr("MOD_PLATEHOLDER"), XorStr("MOD_VANITYPLATES"), XorStr("MOD_TRIMDESIGN"), XorStr("MOD_ORNAMENTS"), XorStr("MOD_DASHBOARD"), XorStr("MOD_DIALDESIGN"), XorStr("MOD_DOORSPEAKERS"), XorStr("MOD_SEATS"), XorStr("MOD_STEERINGWHEELS"), XorStr("MOD_COLUMNSHIFTERLEVERS"), XorStr("MOD_PLAQUES"), XorStr("MOD_SPEAKERS"), XorStr("MOD_TRUNK"), XorStr("MOD_HYDRAULICS"), XorStr("MOD_ENGINEBLOCK"), XorStr("MOD_AIRFILTER"), XorStr("MOD_STRUTS"), XorStr("MOD_ARCHCOVER"), XorStr("MOD_AERIALS"), XorStr("MOD_TRIM"), XorStr("MOD_TANK"), XorStr("MOD_WINDOWS"), "", XorStr("MOD_LIVERY") };
        for (int i = MOD_SPOILERS; i <= MOD_LIVERY; i++)
        {
            BOOL bModOn = VEHICLE::IS_TOGGLE_MOD_ON(vehicle, i);
            if (i == MOD_TIRESMOKE && bModOn == TRUE)
            {
                int tireSmokeColor[3];
                VEHICLE::GET_VEHICLE_TYRE_SMOKE_COLOR(vehicle, &tireSmokeColor[0], &tireSmokeColor[1], &tireSmokeColor[2]);
                pLog->WriteToFile(XorStr("VEHICLE::TOGGLE_VEHICLE_MOD(playerVeh, MOD_TIRESMOKE, TRUE);"), VEHICLE::IS_TOGGLE_MOD_ON(vehicle, i));
                pLog->WriteToFile(XorStr("VEHICLE::SET_VEHICLE_TYRE_SMOKE_COLOR(playerVeh, %u, %u, %u);"), tireSmokeColor[0], tireSmokeColor[1], tireSmokeColor[2]);
            }
            else if (bModOn == TRUE)
            {
                pLog->WriteToFile(XorStr("VEHICLE::TOGGLE_VEHICLE_MOD(playerVeh, %s, TRUE);"), modNames[i]);
            }
            if (VEHICLE::GET_VEHICLE_MOD(vehicle, i) != -1)
                pLog->WriteToFile(XorStr("VEHICLE::SET_VEHICLE_MOD(playerVeh, %s, %i, %s);"), modNames[i], VEHICLE::GET_VEHICLE_MOD(vehicle, i), VEHICLE::GET_VEHICLE_MOD_VARIATION(vehicle, i) ? "TRUE" : "FALSE");
        }
        int pearlescentColor, wheelColor;
        pLog->WriteToFile(XorStr("VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT(playerVeh, \"%s\");"), VEHICLE::GET_VEHICLE_NUMBER_PLATE_TEXT(vehicle));
        pLog->WriteToFile(XorStr("VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(playerVeh, %i);"), VEHICLE::GET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehicle));
        VEHICLE::GET_VEHICLE_EXTRA_COLOURS(vehicle, &pearlescentColor, &wheelColor);
        pLog->WriteToFile(XorStr("VEHICLE::SET_VEHICLE_EXTRA_COLOURS(playerVeh, %u, %u);"), pearlescentColor, wheelColor);
        bool bDumpLightColor = false;
        for (int i = NEON_LEFT; i <= NEON_BACK; i++)
        {
            BOOL bIsLightOn = VEHICLE::_IS_VEHICLE_NEON_LIGHT_ENABLED(vehicle, i);
            if (bIsLightOn == TRUE)
            {
                if (bDumpLightColor == false)
                {
                    int neonColor[3];
                    VEHICLE::_GET_VEHICLE_NEON_LIGHTS_COLOUR(vehicle, &neonColor[0], &neonColor[1], &neonColor[2]);
                    pLog->WriteToFile(XorStr("VEHICLE::_SET_VEHICLE_NEON_LIGHTS_COLOUR(playerVeh, %u, %u, %u);"), neonColor[0], neonColor[1], neonColor[2]);
                    bDumpLightColor = true;
                }
                pLog->WriteToFile(XorStr("VEHICLE::_SET_VEHICLE_NEON_LIGHT_ENABLED(playerVeh, %i, TRUE);"), i);
            }
        }
        if (VEHICLE::IS_VEHICLE_A_CONVERTIBLE(vehicle, 0))
        {
            int convertableState = VEHICLE::GET_CONVERTIBLE_ROOF_STATE(vehicle);
            if (convertableState == 0 || convertableState == 3 || convertableState == 5)
                pLog->WriteToFile(XorStr("VEHICLE::RAISE_CONVERTIBLE_ROOF(playerVeh, TRUE);"));
            else
                pLog->WriteToFile(XorStr("VEHICLE::LOWER_CONVERTIBLE_ROOF(playerVeh, TRUE);"));
        }
        for (int i = 0; i <= 11; i++)
        {
            if (VEHICLE::DOES_EXTRA_EXIST(vehicle, i) == TRUE)
                pLog->WriteToFile(XorStr("VEHICLE::SET_VEHICLE_EXTRA(playerVeh, %i, %s);"), i, VEHICLE::IS_VEHICLE_EXTRA_TURNED_ON(vehicle, i) ? "FALSE" : "TRUE");
        }
        if ((VEHICLE::GET_VEHICLE_LIVERY_COUNT(vehicle) > 1) && VEHICLE::GET_VEHICLE_LIVERY(vehicle) >= 0)
        {
            pLog->WriteToFile(XorStr("VEHICLE::SET_VEHICLE_LIVERY(playerVeh, %i);"), VEHICLE::GET_VEHICLE_LIVERY(vehicle));
        }
        int interiorColor, dashboardColor;
        VEHICLE::GET_VEHICLE_INTERIOR_COLOUR(vehicle, &interiorColor);
        VEHICLE::GET_VEHICLE_DASHBOARD_COLOUR(vehicle, &dashboardColor);
        if (interiorColor != 0)
            pLog->WriteToFile(XorStr("VEHICLE::SET_VEHICLE_INTERIOR_COLOUR(playerVeh, %i);"), interiorColor);
        if (dashboardColor != 0)
            pLog->WriteToFile(XorStr("VEHICLE::SET_VEHICLE_DASHBOARD_COLOUR(playerVeh, %i);"), dashboardColor);
        if (GRAPHICS::_DOES_VEHICLE_HAVE_DECAL(vehicle, 0) == TRUE)
        {
            pLog->WriteToFile(XorStr("AddClanLogoToVehicle(playerVeh, playerPed);"));
        }
    }
}

static void BoostBaseVehicleStats(Vehicle vehicle)
{
    NETWORK::NETWORK_REQUEST_CONTROL_OF_ENTITY(vehicle);
    VEHICLE::SET_VEHICLE_FIXED(vehicle);
    VEHICLE::SET_VEHICLE_DIRT_LEVEL(vehicle, 0.0f);
    VEHICLE::SET_VEHICLE_STRONG(vehicle, TRUE);
    VEHICLE::SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehicle, FALSE);
    VEHICLE::SET_VEHICLE_CAN_BREAK(vehicle, FALSE);
    VEHICLE::SET_VEHICLE_ENGINE_CAN_DEGRADE(vehicle, FALSE);
    VEHICLE::SET_VEHICLE_IS_STOLEN(vehicle, FALSE);
    Hash vehicleModel = ENTITY::GET_ENTITY_MODEL(vehicle);
    if (ENTITY::IS_ENTITY_UPSIDEDOWN(vehicle))
    {
        Vector3 rotation = ENTITY::GET_ENTITY_ROTATION(vehicle, 2);
        ENTITY::SET_ENTITY_ROTATION(vehicle, rotation.x, 0, rotation.z, 2, TRUE);
    }
    if (VEHICLE::IS_VEHICLE_ATTACHED_TO_TRAILER(vehicle))
    {
        Vehicle trailer;
        VEHICLE::GET_VEHICLE_TRAILER_VEHICLE(vehicle, &trailer);
        NETWORK::NETWORK_REQUEST_CONTROL_OF_ENTITY(trailer);
        VEHICLE::SET_VEHICLE_DIRT_LEVEL(trailer, 0.0f);
        //VEHICLE::SET_VEHICLE_FIXED(trailer);
        VEHICLE::SET_VEHICLE_STRONG(trailer, TRUE);
        VEHICLE::SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(trailer, FALSE);
        VEHICLE::SET_VEHICLE_CAN_BREAK(trailer, FALSE);
        VEHICLE::SET_VEHICLE_ENGINE_HEALTH(trailer, 2000.0f);
        VEHICLE::SET_VEHICLE_PETROL_TANK_HEALTH(trailer, 2000.0f);
        VEHICLE::SET_VEHICLE_BODY_HEALTH(trailer, 2000.0f);
    }
    if (VEHICLE::IS_THIS_MODEL_A_CAR(vehicleModel) || VEHICLE::IS_THIS_MODEL_A_BIKE(vehicleModel))
    {
        VEHICLE::SET_VEHICLE_TYRES_CAN_BURST(vehicle, FALSE); //Bulletproof Tires.
        VEHICLE::SET_VEHICLE_MOD_KIT(vehicle, 0); //IDK what this does, but I guess it allows individual mods to be added? It's what the game does before calling SET_VEHICLE_MOD.
        VEHICLE::SET_VEHICLE_HAS_STRONG_AXLES(vehicle, TRUE); //6stronk9meme
        VEHICLE::SET_VEHICLE_MOD(vehicle, MOD_ENGINE, VEHICLE::GET_NUM_VEHICLE_MODS(vehicle, MOD_ENGINE) - 1, FALSE); //6fast9furious
        VEHICLE::SET_VEHICLE_MOD(vehicle, MOD_BRAKES, VEHICLE::GET_NUM_VEHICLE_MODS(vehicle, MOD_BRAKES) - 1, FALSE); //GOTTA STOP FAST
        VEHICLE::SET_VEHICLE_MOD(vehicle, MOD_TRANSMISSION, VEHICLE::GET_NUM_VEHICLE_MODS(vehicle, MOD_TRANSMISSION) - 1, FALSE); //Not when I shift in to MAXIMUM OVERMEME!
        VEHICLE::SET_VEHICLE_MOD(vehicle, MOD_SUSPENSION, VEHICLE::GET_NUM_VEHICLE_MODS(vehicle, MOD_SUSPENSION) - 1, FALSE); //How low can you go?
        VEHICLE::SET_VEHICLE_MOD(vehicle, MOD_ARMOR, VEHICLE::GET_NUM_VEHICLE_MODS(vehicle, MOD_ARMOR) - 1, FALSE); //100% armor.
        VEHICLE::TOGGLE_VEHICLE_MOD(vehicle, MOD_TURBO, TRUE); //Forced induction huehuehue
    }
    VEHICLE::SET_VEHICLE_ENGINE_HEALTH(vehicle, 2000.0f);
    VEHICLE::SET_VEHICLE_PETROL_TANK_HEALTH(vehicle, 2000.0f);
    VEHICLE::SET_VEHICLE_BODY_HEALTH(vehicle, 2000.0f);
}