#pragma once
#include <windows.h>
#include <Psapi.h>
#include <string>
#include <fstream>
#include <stdint.h>
#include <vector>
#include <set>
#include <sstream>
#include <stdio.h>
#include <winnt.h>
#include <Winternl.h>
#include <array>
#include <detver.h>
#include <detours.h>
#include <time.h>
#include "XorStr.h"
#include "KeyHelper.h"

#pragma comment(lib, "detours.lib")
#pragma comment(lib, "Winmm.lib")

#define IsKeyPressed(key) GetAsyncKeyState(key) & 0x8000

using     std::string;

extern DWORD dwImageSize;
extern DWORD64 dwImageBase;

typedef DWORD Void;
typedef DWORD Any;
typedef DWORD uint;
typedef DWORD Hash;
typedef DWORD ScrHandle;
typedef int Entity;
typedef int Player;
typedef int FireId;
typedef int Ped;
typedef int Vehicle;
typedef int Cam;
typedef int CarGenerator;
typedef int Group;
typedef int Train;
typedef int Pickup;
typedef int Object;
typedef int Weapon;
typedef int Interior;
typedef int Texture;
typedef int TextureDict;
typedef int CoverPoint;
typedef int Camera;
typedef int TaskSequence;
typedef int ColourIndex;
typedef int Sphere;
typedef int Blip;

typedef struct
{
    float x;
    DWORD _paddingx;
    float y;
    DWORD _paddingy;
    float z;
    DWORD _paddingz;
} Vector3;

typedef struct
{
    float x;
    float y;
    float z;
} vector3_t;

typedef struct
{
    BYTE red;
    BYTE green;
    BYTE blue;
    BYTE alpha;
} color_t;

typedef unsigned __int64 QWORD;
typedef unsigned __int64* PQWORD;

//RAGE includes
#include "Blip.h"
#include "Types.h"
#include "Hashes.h"
#include "Replay.h"
#include "Log.h"
#include "GetOffsets.h"

//Hack Includes
#include "scrThread.h"
#include "Engine.h"
#include "natives.h"
#include "Hooks.h"
#include "VehicleValues.h"
#include "DrawHelper.h"
#include "StatScripts.h"
#include "RageHelper.h"
#include "NetworkHelper.h"
#include "VehicleHelper.h"