﻿#include "dllmain.h"

inline bool checkFileExists(const std::string& name) {
    struct stat buffer;
    return (stat(name.c_str(), &buffer) == 0);
}

long WINAPI VectoredHandler(EXCEPTION_POINTERS *pException)
{
    if (pException->ExceptionRecord->ExceptionCode == STATUS_ACCESS_VIOLATION)
    {
        if ((DWORD64)pException->ExceptionRecord->ExceptionAddress == gOffsets.ModelCrashLocation)
        {
            pException->ContextRecord->Rip -= 0x10;
            return EXCEPTION_CONTINUE_EXECUTION;
        }
    }
    return EXCEPTION_CONTINUE_SEARCH;
}

typedef char(__fastcall*IsDlcPresent_t)(unsigned int a1);
IsDlcPresent_t orig_IsDlcPresent;
int __fastcall IsDlcPresentHook(unsigned int a1)
{
    if (DLC2::GET_IS_LOADING_SCREEN_ACTIVE())
        goto retn;

    int player = PLAYER::PLAYER_ID();
    int playerPed = PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(player);

    static bool bNeedsToFinishTP = false;
    if (bNeedsToFinishTP == true)
        goto findZCoords;

    if (ENTITY::DOES_ENTITY_EXIST(playerPed) && !ENTITY::IS_ENTITY_DEAD(playerPed))
    {
        DWORD playerVeh = NULL;

        if (PED::IS_PED_IN_ANY_VEHICLE(playerPed, FALSE))
            playerVeh = PED::GET_VEHICLE_PED_IS_USING(playerPed);

        int maxHealth = PED::GET_PED_MAX_HEALTH(playerPed), health = ENTITY::GET_ENTITY_HEALTH(playerPed);
        if (health < maxHealth && health > 1)
            ENTITY::SET_ENTITY_HEALTH(playerPed, maxHealth);
        int armorDelta = PLAYER::GET_PLAYER_MAX_ARMOUR(player) - PED::GET_PED_ARMOUR(playerPed);
        if (armorDelta > 0)
            PED::ADD_ARMOUR_TO_PED(playerPed, armorDelta);
        if (PED::CAN_PED_RAGDOLL(playerPed))
        {
            PED::SET_PED_CAN_RAGDOLL(playerPed, FALSE);
            PED::SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(playerPed, FALSE);
        }

        if (*ENGINE::GET_GLOBAL_PTR(BUNKER_RESEARCH_GLOBAL) == false)
            *ENGINE::GET_GLOBAL_PTR(BUNKER_RESEARCH_GLOBAL) = true;

        static bool bMenuActive, bF3Pressed = false;
        if (isKeyPressedOnce(bF3Pressed, VK_F3))
        {
            bMenuActive = !bMenuActive;
        }
        bool bReset = false;
        if (bMenuActive)
        {
            static int iSelectedPlayer = 0;
            static bool bPgUpPressed, bPgDwnPressed = false;
            if (isKeyPressedOnce(bPgUpPressed, VK_PRIOR))
            {
                iSelectedPlayer--;
                CheckPlayer(iSelectedPlayer, false);
            }
            if (isKeyPressedOnce(bPgDwnPressed, VK_NEXT))
            {
                iSelectedPlayer++;
                CheckPlayer(iSelectedPlayer, true);
            }
            CheckPlayer(iSelectedPlayer, !bPgUpPressed);
            int iLineNum = 0;
            Ped selectedPed = NULL;
            for (Player playerIterator = 0; playerIterator < MAX_PLAYERS; playerIterator++)
            {
                bool bSelectedPed = (playerIterator == iSelectedPlayer);
                Ped pedIterator = PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(playerIterator);
                if (bSelectedPed)
                    selectedPed = pedIterator;
                if (ENTITY::DOES_ENTITY_EXIST(pedIterator))
                {
                    draw_menu_line(PLAYER::GET_PLAYER_NAME(playerIterator), 150.0f, 4.0f, 25.0f + iLineNum * 13.0f, 350.0f, 0.0f, false, false, bSelectedPed, false);
                    iLineNum++;
                }
            }

            static bool bDecimalPressed = false;
            if (isKeyPressedOnce(bDecimalPressed, VK_DECIMAL))
            {
                if (selectedPed == playerPed)
                {
                    if (IsKeyPressed(VK_RCONTROL))
                    {
                        //This will cycle our PED entity to remove any attached garbage to us. Select an outfit to fix your model.
                        PLAYER::SET_PLAYER_MODEL(PLAYER::PLAYER_ID(), ENTITY::GET_ENTITY_MODEL(playerPed));
                    }
                    else
                    {
                        Vehicle vehicle = PED::GET_VEHICLE_PED_IS_IN(playerPed, TRUE);
                        for (int i = SEAT_BACKPASSENGER; i >= SEAT_DRIVER; i--)
                        {
                            PED::SET_PED_INTO_VEHICLE(playerPed, vehicle, i);
                        }
                    }
                }
                else
                {
                    if (PED::IS_PED_IN_ANY_VEHICLE(selectedPed, FALSE))
                    {
                        Vehicle selectedVehicle = PED::GET_VEHICLE_PED_IS_USING(selectedPed);
                        for (int i = SEAT_BACKPASSENGER; i >= SEAT_DRIVER; i--)
                        {
                            PED::SET_PED_INTO_VEHICLE(playerPed, selectedVehicle, i);
                        }
                    }
                }
            }
            //Teleport to selected player on the menu.
            static bool bNumpad0Pressed = false;
            if (isKeyPressedOnce(bNumpad0Pressed, VK_NUMPAD0) && selectedPed != playerPed)
            {
                Entity e = playerPed;
                if (playerVeh != NULL)
                    e = playerVeh;

                Vector3 playerPosition = ENTITY::GET_ENTITY_COORDS(selectedPed, FALSE);
                ENTITY::SET_ENTITY_COORDS_NO_OFFSET(e, playerPosition.x, playerPosition.y, playerPosition.z + 1, FALSE, FALSE, TRUE);
            }
            //Explode loby.
            if (IsKeyPressed(VK_NUMPAD1))
            {
                if (IsPlayerFriend(iSelectedPlayer) == FALSE)
                {
                    if (IsKeyPressed(VK_RCONTROL))
                    {
                        if (iSelectedPlayer != player)
                        {
                            if (PED::IS_PED_IN_ANY_VEHICLE(selectedPed, FALSE))
                            {
                                uint64_t args[9] = { 325218053, iSelectedPlayer, iSelectedPlayer, 0, 0, 0, 0, -1, GAMEPLAY::GET_FRAME_COUNT() };
                                ENGINE::TRIGGER_SCRIPT_EVENT(1, &args[0], 9, (1 << iSelectedPlayer));
                            }
                            BlameExplosionOnPed(selectedPed, selectedPed);
                        }
                        else
                        {
                            Vector3 playerPosition = ENTITY::GET_ENTITY_COORDS(selectedPed, FALSE);
                            FIRE::ADD_EXPLOSION(playerPosition.x, playerPosition.y, playerPosition.z, EXPLOSION_STICKYBOMB, 1.0f, FALSE, TRUE, 0.0f);
                        }
                    }
                    else if (IsKeyPressed(VK_RMENU))
                    {
                        BlameExplosionOnPed(selectedPed, playerPed);
                    }
                    else
                    {
                        static int playerIterator = 0;
                        for (int i = 0; i < 5; i++)
                        {
                            Ped pedIterator = PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(playerIterator);
                            if (ENTITY::DOES_ENTITY_EXIST(pedIterator) && !IsPlayerFriend(playerIterator) && playerIterator != player && iSelectedPlayer != playerIterator)
                            {
                                AI::CLEAR_PED_TASKS_IMMEDIATELY(pedIterator);
                                BlameExplosionOnPed(selectedPed, pedIterator);
                            }
                            playerIterator++;
                            if (playerIterator > MAX_PLAYERS)
                                playerIterator = 0;
                        }
                    }
                }
            }
            //Clone car.
            static bool bNumpad2Pressed = false;
            if (isKeyPressedOnce(bNumpad2Pressed, VK_NUMPAD2))
            {
                if (PED::IS_PED_IN_ANY_VEHICLE(selectedPed, FALSE))
                {
                    Vehicle clonedVeh = ClonePedVehicle(selectedPed);
                    BoostBaseVehicleStats(clonedVeh); //Gotta go fast
                }
            }
            static bool bNumpad3KickPressed;
            if (isKeyPressedOnce(bNumpad3KickPressed, VK_NUMPAD3))
            {
                if (selectedPed == playerPed)
                {
                    CObjectInterface* objIF = gOffsets.replayInterface->pCObjectInterface;
                    int numObj = objIF->iMaxObjects;
                    Object weapon = WEAPON::GET_CURRENT_PED_WEAPON_ENTITY_INDEX(playerPed);
                    for (int i = 0; i < numObj; i++)
                    {
                        CObject* pCObject = objIF->get_object(i);
                        if (pCObject == nullptr)
                            continue;

                        Object obj = ptr_to_handle(pCObject);
                        if (obj == 0)
                            break;

                        if (!ENTITY::IS_ENTITY_ATTACHED_TO_ENTITY(playerPed, obj) || weapon == obj)
                            continue;

                        NETWORK::NETWORK_REQUEST_CONTROL_OF_ENTITY(obj);
                        ENTITY::SET_ENTITY_AS_MISSION_ENTITY(obj, 1, 1);
                        NETWORK::NETWORK_FADE_OUT_ENTITY(obj, 1, 0);

                        ENTITY::DETACH_ENTITY(obj, 1, 1);
                        ENTITY::SET_ENTITY_COLLISION(obj, FALSE, FALSE);
                        ENTITY::SET_ENTITY_COORDS(obj, 0, 0, -100, 0, 0, 0, 0);
                        ENTITY::DELETE_ENTITY(&obj);
                        if (ENTITY::DOES_ENTITY_EXIST(obj))
                            ENTITY::SET_ENTITY_AS_NO_LONGER_NEEDED(&obj);

                        draw_notification_red(XorStr("CHAR_DEFAULT"), "", XorStr("Removed attached object."), "");
                        pLog->WriteToFile(XorStr("Removed attached object."));
                    }
                }
                else if (IsPlayerFriend(iSelectedPlayer) == FALSE)
                {
                    uint64_t args[4] = { -1289983205, iSelectedPlayer, -1, 0 };
                    ENGINE::TRIGGER_SCRIPT_EVENT(1, &args[0], 4, (1 << iSelectedPlayer));
                }
            }
            static bool bNumpad7KickPressed;
            if (isKeyPressedOnce(bNumpad7KickPressed, VK_NUMPAD7))
            {
                if (!IsPlayerFriend(iSelectedPlayer))
                {
                    uint64_t args[9] = { 325218053, iSelectedPlayer, iSelectedPlayer, 0, 0, 0, 0, -1, GAMEPLAY::GET_FRAME_COUNT() };
                    ENGINE::TRIGGER_SCRIPT_EVENT(1, &args[0], 9, (1 << iSelectedPlayer));
                }
            }
            //Persist weapons to INI.
            static bool bF9Pressed = false;
            if (isKeyPressedOnce(bF9Pressed, VK_F9))
            {
                Hash arrayOfWeapons[] = { WEAPON_ADVANCEDRIFLE, WEAPON_APPISTOL, WEAPON_ASSAULTRIFLE, WEAPON_ASSAULTRIFLE_MK2, WEAPON_ASSAULTSHOTGUN, WEAPON_ASSAULTSMG, WEAPON_BALL, WEAPON_BAT, WEAPON_BATTLEAXE, WEAPON_BOTTLE, WEAPON_BULLPUPRIFLE, WEAPON_BULLPUPRIFLE_MK2, WEAPON_BULLPUPSHOTGUN, WEAPON_BZGAS, WEAPON_CARBINERIFLE, WEAPON_CARBINERIFLE_MK2, WEAPON_COMBATMG, WEAPON_COMBATMG_MK2, WEAPON_COMBATPDW, WEAPON_COMBATPISTOL, WEAPON_COMPACTLAUNCHER, WEAPON_COMPACTRIFLE, WEAPON_CROWBAR, WEAPON_DAGGER, WEAPON_DOUBLEACTION, WEAPON_FIREEXTINGUISHER, WEAPON_FIREWORK, WEAPON_FLARE, WEAPON_FLAREGUN, WEAPON_FLASHLIGHT, WEAPON_GOLFCLUB, WEAPON_GRENADE, WEAPON_GRENADELAUNCHER, WEAPON_GRENADELAUNCHER_SMOKE, WEAPON_GUSENBERG, WEAPON_HAMMER, WEAPON_HATCHET, WEAPON_HEAVYPISTOL, WEAPON_HEAVYSHOTGUN, WEAPON_HEAVYSNIPER, WEAPON_HEAVYSNIPER_MK2, WEAPON_HOMINGLAUNCHER, WEAPON_KNIFE, WEAPON_KNUCKLE, WEAPON_MACHETE, WEAPON_MACHINEPISTOL, WEAPON_MARKSMANPISTOL, WEAPON_MARKSMANRIFLE, WEAPON_MARKSMANRIFLE_MK2, WEAPON_MG, WEAPON_MICROSMG, WEAPON_MINIGUN, WEAPON_MINISMG, WEAPON_MOLOTOV, WEAPON_MUSKET, WEAPON_NIGHTSTICK, WEAPON_PETROLCAN, WEAPON_PIPEBOMB, WEAPON_PISTOL50, WEAPON_PISTOL, WEAPON_PISTOL_MK2, WEAPON_POOLCUE, WEAPON_PROXMINE, WEAPON_PUMPSHOTGUN, WEAPON_PUMPSHOTGUN_MK2, WEAPON_RAILGUN, WEAPON_REVOLVER, WEAPON_REVOLVER_MK2, WEAPON_RPG, WEAPON_SAWNOFFSHOTGUN, WEAPON_SMG, WEAPON_SMG_MK2, WEAPON_SMOKEGRENADE, WEAPON_SNIPERRIFLE, WEAPON_SNOWBALL, WEAPON_SNSPISTOL, WEAPON_SNSPISTOL_MK2, WEAPON_SPECIALCARBINE, WEAPON_SPECIALCARBINE_MK2, WEAPON_STICKYBOMB, WEAPON_STINGER, WEAPON_STONE_HATCHET, WEAPON_STUNGUN, WEAPON_SWITCHBLADE, WEAPON_VINTAGEPISTOL, WEAPON_WRENCH, WEAPON_RAYCARBINE, WEAPON_RAYPISTOL, WEAPON_RAYMINIGUN };
                Hash arrayOfComponents[] = { COMPONENT_ADVANCEDRIFLE_CLIP_01, COMPONENT_ADVANCEDRIFLE_CLIP_02, COMPONENT_ADVANCEDRIFLE_VARMOD_LUXE, COMPONENT_APPISTOL_CLIP_01, COMPONENT_APPISTOL_CLIP_02, COMPONENT_APPISTOL_VARMOD_LUXE, COMPONENT_ASSAULTRIFLE_CLIP_01, COMPONENT_ASSAULTRIFLE_CLIP_02, COMPONENT_ASSAULTRIFLE_CLIP_03, COMPONENT_ASSAULTRIFLE_MK2_CAMO, COMPONENT_ASSAULTRIFLE_MK2_CAMO_02, COMPONENT_ASSAULTRIFLE_MK2_CAMO_03, COMPONENT_ASSAULTRIFLE_MK2_CAMO_04, COMPONENT_ASSAULTRIFLE_MK2_CAMO_05, COMPONENT_ASSAULTRIFLE_MK2_CAMO_06, COMPONENT_ASSAULTRIFLE_MK2_CAMO_07, COMPONENT_ASSAULTRIFLE_MK2_CAMO_08, COMPONENT_ASSAULTRIFLE_MK2_CAMO_09, COMPONENT_ASSAULTRIFLE_MK2_CAMO_10, COMPONENT_ASSAULTRIFLE_MK2_CAMO_IND_01, COMPONENT_ASSAULTRIFLE_MK2_CLIP_01, COMPONENT_ASSAULTRIFLE_MK2_CLIP_02, COMPONENT_ASSAULTRIFLE_MK2_CLIP_ARMORPIERCING, COMPONENT_ASSAULTRIFLE_MK2_CLIP_FMJ, COMPONENT_ASSAULTRIFLE_MK2_CLIP_INCENDIARY, COMPONENT_ASSAULTRIFLE_MK2_CLIP_TRACER, COMPONENT_ASSAULTRIFLE_VARMOD_LUXE, COMPONENT_ASSAULTSHOTGUN_CLIP_01, COMPONENT_ASSAULTSHOTGUN_CLIP_02, COMPONENT_ASSAULTSMG_CLIP_01, COMPONENT_ASSAULTSMG_CLIP_02, COMPONENT_ASSAULTSMG_VARMOD_LOWRIDER, COMPONENT_AT_AR_AFGRIP, COMPONENT_AT_AR_AFGRIP_02, COMPONENT_AT_AR_BARREL_01, COMPONENT_AT_AR_BARREL_02, COMPONENT_AT_AR_FLSH, COMPONENT_AT_AR_SUPP, COMPONENT_AT_AR_SUPP_02, COMPONENT_AT_BP_BARREL_01, COMPONENT_AT_BP_BARREL_02, COMPONENT_AT_CR_BARREL_01, COMPONENT_AT_CR_BARREL_02, COMPONENT_AT_MG_BARREL_01, COMPONENT_AT_MG_BARREL_02, COMPONENT_AT_MRFL_BARREL_01, COMPONENT_AT_MRFL_BARREL_02, COMPONENT_AT_MUZZLE_01, COMPONENT_AT_MUZZLE_02, COMPONENT_AT_MUZZLE_03, COMPONENT_AT_MUZZLE_04, COMPONENT_AT_MUZZLE_05, COMPONENT_AT_MUZZLE_06, COMPONENT_AT_MUZZLE_07, COMPONENT_AT_MUZZLE_08, COMPONENT_AT_MUZZLE_09, COMPONENT_AT_PI_COMP, COMPONENT_AT_PI_COMP_02, COMPONENT_AT_PI_COMP_03, COMPONENT_AT_PI_FLSH, COMPONENT_AT_PI_FLSH_02, COMPONENT_AT_PI_FLSH_03, COMPONENT_AT_PI_RAIL, COMPONENT_AT_PI_RAIL_02, COMPONENT_AT_PI_SUPP, COMPONENT_AT_PI_SUPP_02, COMPONENT_AT_SB_BARREL_01, COMPONENT_AT_SB_BARREL_02, COMPONENT_AT_SCOPE_LARGE, COMPONENT_AT_SCOPE_LARGE_FIXED_ZOOM, COMPONENT_AT_SCOPE_LARGE_FIXED_ZOOM_MK2, COMPONENT_AT_SCOPE_LARGE_MK2, COMPONENT_AT_SCOPE_MACRO, COMPONENT_AT_SCOPE_MACRO_02, COMPONENT_AT_SCOPE_MACRO_02_MK2, COMPONENT_AT_SCOPE_MACRO_02_SMG_MK2, COMPONENT_AT_SCOPE_MACRO_MK2, COMPONENT_AT_SCOPE_MAX, COMPONENT_AT_SCOPE_MEDIUM, COMPONENT_AT_SCOPE_MEDIUM_MK2, COMPONENT_AT_SCOPE_NV, COMPONENT_AT_SCOPE_SMALL, COMPONENT_AT_SCOPE_SMALL, COMPONENT_AT_SCOPE_SMALL_02, COMPONENT_AT_SCOPE_SMALL_MK2, COMPONENT_AT_SCOPE_SMALL_SMG_MK2, COMPONENT_AT_SCOPE_THERMAL, COMPONENT_AT_SC_BARREL_01, COMPONENT_AT_SC_BARREL_02, COMPONENT_AT_SIGHTS, COMPONENT_AT_SIGHTS_SMG, COMPONENT_AT_SR_BARREL_01, COMPONENT_AT_SR_BARREL_02, COMPONENT_AT_SR_SUPP, COMPONENT_AT_SR_SUPP_03, COMPONENT_BULLPUPRIFLE_CLIP_01, COMPONENT_BULLPUPRIFLE_CLIP_02, COMPONENT_BULLPUPRIFLE_MK2_CAMO, COMPONENT_BULLPUPRIFLE_MK2_CAMO_02, COMPONENT_BULLPUPRIFLE_MK2_CAMO_03, COMPONENT_BULLPUPRIFLE_MK2_CAMO_04, COMPONENT_BULLPUPRIFLE_MK2_CAMO_05, COMPONENT_BULLPUPRIFLE_MK2_CAMO_06, COMPONENT_BULLPUPRIFLE_MK2_CAMO_07, COMPONENT_BULLPUPRIFLE_MK2_CAMO_08, COMPONENT_BULLPUPRIFLE_MK2_CAMO_09, COMPONENT_BULLPUPRIFLE_MK2_CAMO_10, COMPONENT_BULLPUPRIFLE_MK2_CAMO_IND_01, COMPONENT_BULLPUPRIFLE_MK2_CLIP_01, COMPONENT_BULLPUPRIFLE_MK2_CLIP_02, COMPONENT_BULLPUPRIFLE_MK2_CLIP_ARMORPIERCING, COMPONENT_BULLPUPRIFLE_MK2_CLIP_FMJ, COMPONENT_BULLPUPRIFLE_MK2_CLIP_INCENDIARY, COMPONENT_BULLPUPRIFLE_MK2_CLIP_TRACER, COMPONENT_BULLPUPRIFLE_VARMOD_LOW, COMPONENT_CARBINERIFLE_CLIP_01, COMPONENT_CARBINERIFLE_CLIP_02, COMPONENT_CARBINERIFLE_CLIP_03, COMPONENT_CARBINERIFLE_MK2_CAMO, COMPONENT_CARBINERIFLE_MK2_CAMO_02, COMPONENT_CARBINERIFLE_MK2_CAMO_03, COMPONENT_CARBINERIFLE_MK2_CAMO_04, COMPONENT_CARBINERIFLE_MK2_CAMO_05, COMPONENT_CARBINERIFLE_MK2_CAMO_06, COMPONENT_CARBINERIFLE_MK2_CAMO_07, COMPONENT_CARBINERIFLE_MK2_CAMO_08, COMPONENT_CARBINERIFLE_MK2_CAMO_09, COMPONENT_CARBINERIFLE_MK2_CAMO_10, COMPONENT_CARBINERIFLE_MK2_CAMO_IND_01, COMPONENT_CARBINERIFLE_MK2_CLIP_01, COMPONENT_CARBINERIFLE_MK2_CLIP_02, COMPONENT_CARBINERIFLE_MK2_CLIP_ARMORPIERCING, COMPONENT_CARBINERIFLE_MK2_CLIP_FMJ, COMPONENT_CARBINERIFLE_MK2_CLIP_INCENDIARY, COMPONENT_CARBINERIFLE_MK2_CLIP_TRACER, COMPONENT_CARBINERIFLE_VARMOD_LUXE, COMPONENT_COMBATMG_CLIP_01, COMPONENT_COMBATMG_CLIP_02, COMPONENT_COMBATMG_MK2_CAMO, COMPONENT_COMBATMG_MK2_CAMO_02, COMPONENT_COMBATMG_MK2_CAMO_03, COMPONENT_COMBATMG_MK2_CAMO_04, COMPONENT_COMBATMG_MK2_CAMO_05, COMPONENT_COMBATMG_MK2_CAMO_06, COMPONENT_COMBATMG_MK2_CAMO_07, COMPONENT_COMBATMG_MK2_CAMO_08, COMPONENT_COMBATMG_MK2_CAMO_09, COMPONENT_COMBATMG_MK2_CAMO_10, COMPONENT_COMBATMG_MK2_CAMO_IND_01, COMPONENT_COMBATMG_MK2_CLIP_01, COMPONENT_COMBATMG_MK2_CLIP_02, COMPONENT_COMBATMG_MK2_CLIP_ARMORPIERCING, COMPONENT_COMBATMG_MK2_CLIP_FMJ, COMPONENT_COMBATMG_MK2_CLIP_INCENDIARY, COMPONENT_COMBATMG_MK2_CLIP_TRACER, COMPONENT_COMBATMG_VARMOD_LOWRIDER, COMPONENT_COMBATPDW_CLIP_01, COMPONENT_COMBATPDW_CLIP_02, COMPONENT_COMBATPDW_CLIP_03, COMPONENT_COMBATPISTOL_CLIP_01, COMPONENT_COMBATPISTOL_CLIP_02, COMPONENT_COMBATPISTOL_VARMOD_LOWRIDER, COMPONENT_COMPACTRIFLE_CLIP_01, COMPONENT_COMPACTRIFLE_CLIP_02, COMPONENT_COMPACTRIFLE_CLIP_03, COMPONENT_GRENADELAUNCHER_CLIP_01, COMPONENT_GUSENBERG_CLIP_01, COMPONENT_GUSENBERG_CLIP_02, COMPONENT_HEAVYPISTOL_CLIP_01, COMPONENT_HEAVYPISTOL_CLIP_02, COMPONENT_HEAVYPISTOL_VARMOD_LUXE, COMPONENT_HEAVYSHOTGUN_CLIP_01, COMPONENT_HEAVYSHOTGUN_CLIP_02, COMPONENT_HEAVYSHOTGUN_CLIP_03, COMPONENT_HEAVYSNIPER_CLIP_01, COMPONENT_HEAVYSNIPER_MK2_CAMO, COMPONENT_HEAVYSNIPER_MK2_CAMO_02, COMPONENT_HEAVYSNIPER_MK2_CAMO_03, COMPONENT_HEAVYSNIPER_MK2_CAMO_04, COMPONENT_HEAVYSNIPER_MK2_CAMO_05, COMPONENT_HEAVYSNIPER_MK2_CAMO_06, COMPONENT_HEAVYSNIPER_MK2_CAMO_07, COMPONENT_HEAVYSNIPER_MK2_CAMO_08, COMPONENT_HEAVYSNIPER_MK2_CAMO_09, COMPONENT_HEAVYSNIPER_MK2_CAMO_10, COMPONENT_HEAVYSNIPER_MK2_CAMO_IND_01, COMPONENT_HEAVYSNIPER_MK2_CLIP_01, COMPONENT_HEAVYSNIPER_MK2_CLIP_02, COMPONENT_HEAVYSNIPER_MK2_CLIP_ARMORPIERCING, COMPONENT_HEAVYSNIPER_MK2_CLIP_EXPLOSIVE, COMPONENT_HEAVYSNIPER_MK2_CLIP_FMJ, COMPONENT_HEAVYSNIPER_MK2_CLIP_INCENDIARY, COMPONENT_KNUCKLE_VARMOD_BALLAS, COMPONENT_KNUCKLE_VARMOD_BASE, COMPONENT_KNUCKLE_VARMOD_DIAMOND, COMPONENT_KNUCKLE_VARMOD_DOLLAR, COMPONENT_KNUCKLE_VARMOD_HATE, COMPONENT_KNUCKLE_VARMOD_KING, COMPONENT_KNUCKLE_VARMOD_LOVE, COMPONENT_KNUCKLE_VARMOD_PIMP, COMPONENT_KNUCKLE_VARMOD_PLAYER, COMPONENT_KNUCKLE_VARMOD_VAGOS, COMPONENT_MACHINEPISTOL_CLIP_01, COMPONENT_MACHINEPISTOL_CLIP_02, COMPONENT_MACHINEPISTOL_CLIP_03, COMPONENT_MARKSMANRIFLE_CLIP_01, COMPONENT_MARKSMANRIFLE_CLIP_02, COMPONENT_MARKSMANRIFLE_MK2_CAMO, COMPONENT_MARKSMANRIFLE_MK2_CAMO_02, COMPONENT_MARKSMANRIFLE_MK2_CAMO_03, COMPONENT_MARKSMANRIFLE_MK2_CAMO_04, COMPONENT_MARKSMANRIFLE_MK2_CAMO_05, COMPONENT_MARKSMANRIFLE_MK2_CAMO_06, COMPONENT_MARKSMANRIFLE_MK2_CAMO_07, COMPONENT_MARKSMANRIFLE_MK2_CAMO_08, COMPONENT_MARKSMANRIFLE_MK2_CAMO_09, COMPONENT_MARKSMANRIFLE_MK2_CAMO_10, COMPONENT_MARKSMANRIFLE_MK2_CAMO_IND_01, COMPONENT_MARKSMANRIFLE_MK2_CLIP_01, COMPONENT_MARKSMANRIFLE_MK2_CLIP_02, COMPONENT_MARKSMANRIFLE_MK2_CLIP_ARMORPIERCING, COMPONENT_MARKSMANRIFLE_MK2_CLIP_FMJ, COMPONENT_MARKSMANRIFLE_MK2_CLIP_INCENDIARY, COMPONENT_MARKSMANRIFLE_MK2_CLIP_TRACER, COMPONENT_MARKSMANRIFLE_VARMOD_LUXE, COMPONENT_MG_CLIP_01, COMPONENT_MG_CLIP_02, COMPONENT_MG_VARMOD_LOWRIDER, COMPONENT_MICROSMG_CLIP_01, COMPONENT_MICROSMG_CLIP_02, COMPONENT_MICROSMG_VARMOD_LUXE, COMPONENT_MINISMG_CLIP_01, COMPONENT_MINISMG_CLIP_02, COMPONENT_PISTOL50_CLIP_01, COMPONENT_PISTOL50_CLIP_02, COMPONENT_PISTOL50_VARMOD_LUXE, COMPONENT_PISTOL_CLIP_01, COMPONENT_PISTOL_CLIP_02, COMPONENT_PISTOL_MK2_CAMO, COMPONENT_PISTOL_MK2_CAMO_02, COMPONENT_PISTOL_MK2_CAMO_02_SLIDE, COMPONENT_PISTOL_MK2_CAMO_03, COMPONENT_PISTOL_MK2_CAMO_03_SLIDE, COMPONENT_PISTOL_MK2_CAMO_04, COMPONENT_PISTOL_MK2_CAMO_04_SLIDE, COMPONENT_PISTOL_MK2_CAMO_05, COMPONENT_PISTOL_MK2_CAMO_05_SLIDE, COMPONENT_PISTOL_MK2_CAMO_06, COMPONENT_PISTOL_MK2_CAMO_06_SLIDE, COMPONENT_PISTOL_MK2_CAMO_07, COMPONENT_PISTOL_MK2_CAMO_07_SLIDE, COMPONENT_PISTOL_MK2_CAMO_08, COMPONENT_PISTOL_MK2_CAMO_08_SLIDE, COMPONENT_PISTOL_MK2_CAMO_09, COMPONENT_PISTOL_MK2_CAMO_09_SLIDE, COMPONENT_PISTOL_MK2_CAMO_10, COMPONENT_PISTOL_MK2_CAMO_10_SLIDE, COMPONENT_PISTOL_MK2_CAMO_IND_01, COMPONENT_PISTOL_MK2_CAMO_IND_01_SLIDE, COMPONENT_PISTOL_MK2_CAMO_SLIDE, COMPONENT_PISTOL_MK2_CLIP_01, COMPONENT_PISTOL_MK2_CLIP_02, COMPONENT_PISTOL_MK2_CLIP_FMJ, COMPONENT_PISTOL_MK2_CLIP_HOLLOWPOINT, COMPONENT_PISTOL_MK2_CLIP_INCENDIARY, COMPONENT_PISTOL_MK2_CLIP_TRACER, COMPONENT_PISTOL_VARMOD_LUXE, COMPONENT_PUMPSHOTGUN_MK2_CAMO, COMPONENT_PUMPSHOTGUN_MK2_CAMO_02, COMPONENT_PUMPSHOTGUN_MK2_CAMO_03, COMPONENT_PUMPSHOTGUN_MK2_CAMO_04, COMPONENT_PUMPSHOTGUN_MK2_CAMO_05, COMPONENT_PUMPSHOTGUN_MK2_CAMO_06, COMPONENT_PUMPSHOTGUN_MK2_CAMO_07, COMPONENT_PUMPSHOTGUN_MK2_CAMO_08, COMPONENT_PUMPSHOTGUN_MK2_CAMO_09, COMPONENT_PUMPSHOTGUN_MK2_CAMO_10, COMPONENT_PUMPSHOTGUN_MK2_CAMO_IND_01, COMPONENT_PUMPSHOTGUN_MK2_CLIP_01, COMPONENT_PUMPSHOTGUN_MK2_CLIP_ARMORPIERCING, COMPONENT_PUMPSHOTGUN_MK2_CLIP_EXPLOSIVE, COMPONENT_PUMPSHOTGUN_MK2_CLIP_HOLLOWPOINT, COMPONENT_PUMPSHOTGUN_MK2_CLIP_INCENDIARY, COMPONENT_PUMPSHOTGUN_VARMOD_LOWRIDER, COMPONENT_REVOLVER_CLIP_01, COMPONENT_REVOLVER_MK2_CAMO, COMPONENT_REVOLVER_MK2_CAMO_02, COMPONENT_REVOLVER_MK2_CAMO_03, COMPONENT_REVOLVER_MK2_CAMO_04, COMPONENT_REVOLVER_MK2_CAMO_05, COMPONENT_REVOLVER_MK2_CAMO_06, COMPONENT_REVOLVER_MK2_CAMO_07, COMPONENT_REVOLVER_MK2_CAMO_08, COMPONENT_REVOLVER_MK2_CAMO_09, COMPONENT_REVOLVER_MK2_CAMO_10, COMPONENT_REVOLVER_MK2_CAMO_IND_01, COMPONENT_REVOLVER_MK2_CLIP_01, COMPONENT_REVOLVER_MK2_CLIP_FMJ, COMPONENT_REVOLVER_MK2_CLIP_HOLLOWPOINT, COMPONENT_REVOLVER_MK2_CLIP_INCENDIARY, COMPONENT_REVOLVER_MK2_CLIP_TRACER, COMPONENT_REVOLVER_VARMOD_BOSS, COMPONENT_REVOLVER_VARMOD_GOON, COMPONENT_SAWNOFFSHOTGUN_VARMOD_LUXE, COMPONENT_SMG_CLIP_01, COMPONENT_SMG_CLIP_02, COMPONENT_SMG_CLIP_03, COMPONENT_SMG_MK2_CAMO, COMPONENT_SMG_MK2_CAMO_02, COMPONENT_SMG_MK2_CAMO_03, COMPONENT_SMG_MK2_CAMO_04, COMPONENT_SMG_MK2_CAMO_05, COMPONENT_SMG_MK2_CAMO_06, COMPONENT_SMG_MK2_CAMO_07, COMPONENT_SMG_MK2_CAMO_08, COMPONENT_SMG_MK2_CAMO_09, COMPONENT_SMG_MK2_CAMO_10, COMPONENT_SMG_MK2_CAMO_IND_01, COMPONENT_SMG_MK2_CLIP_01, COMPONENT_SMG_MK2_CLIP_02, COMPONENT_SMG_MK2_CLIP_FMJ, COMPONENT_SMG_MK2_CLIP_HOLLOWPOINT, COMPONENT_SMG_MK2_CLIP_INCENDIARY, COMPONENT_SMG_MK2_CLIP_TRACER, COMPONENT_SMG_VARMOD_LUXE, COMPONENT_SNIPERRIFLE_CLIP_01, COMPONENT_SNIPERRIFLE_VARMOD_LUXE, COMPONENT_SNSPISTOL_CLIP_01, COMPONENT_SNSPISTOL_CLIP_02, COMPONENT_SNSPISTOL_MK2_CAMO, COMPONENT_SNSPISTOL_MK2_CAMO_02, COMPONENT_SNSPISTOL_MK2_CAMO_02_SLIDE, COMPONENT_SNSPISTOL_MK2_CAMO_03, COMPONENT_SNSPISTOL_MK2_CAMO_03_SLIDE, COMPONENT_SNSPISTOL_MK2_CAMO_04, COMPONENT_SNSPISTOL_MK2_CAMO_04_SLIDE, COMPONENT_SNSPISTOL_MK2_CAMO_05, COMPONENT_SNSPISTOL_MK2_CAMO_05_SLIDE, COMPONENT_SNSPISTOL_MK2_CAMO_06, COMPONENT_SNSPISTOL_MK2_CAMO_06_SLIDE, COMPONENT_SNSPISTOL_MK2_CAMO_07, COMPONENT_SNSPISTOL_MK2_CAMO_07_SLIDE, COMPONENT_SNSPISTOL_MK2_CAMO_08, COMPONENT_SNSPISTOL_MK2_CAMO_08_SLIDE, COMPONENT_SNSPISTOL_MK2_CAMO_09, COMPONENT_SNSPISTOL_MK2_CAMO_09_SLIDE, COMPONENT_SNSPISTOL_MK2_CAMO_10, COMPONENT_SNSPISTOL_MK2_CAMO_10_SLIDE, COMPONENT_SNSPISTOL_MK2_CAMO_IND_01, COMPONENT_SNSPISTOL_MK2_CAMO_IND_01_SLIDE, COMPONENT_SNSPISTOL_MK2_CAMO_SLIDE, COMPONENT_SNSPISTOL_MK2_CLIP_01, COMPONENT_SNSPISTOL_MK2_CLIP_02, COMPONENT_SNSPISTOL_MK2_CLIP_FMJ, COMPONENT_SNSPISTOL_MK2_CLIP_HOLLOWPOINT, COMPONENT_SNSPISTOL_MK2_CLIP_INCENDIARY, COMPONENT_SNSPISTOL_MK2_CLIP_TRACER, COMPONENT_SNSPISTOL_VARMOD_LOWRIDER, COMPONENT_SPECIALCARBINE_CLIP_01, COMPONENT_SPECIALCARBINE_CLIP_02, COMPONENT_SPECIALCARBINE_CLIP_03, COMPONENT_SPECIALCARBINE_MK2_CAMO, COMPONENT_SPECIALCARBINE_MK2_CAMO_02, COMPONENT_SPECIALCARBINE_MK2_CAMO_03, COMPONENT_SPECIALCARBINE_MK2_CAMO_04, COMPONENT_SPECIALCARBINE_MK2_CAMO_05, COMPONENT_SPECIALCARBINE_MK2_CAMO_06, COMPONENT_SPECIALCARBINE_MK2_CAMO_07, COMPONENT_SPECIALCARBINE_MK2_CAMO_08, COMPONENT_SPECIALCARBINE_MK2_CAMO_09, COMPONENT_SPECIALCARBINE_MK2_CAMO_10, COMPONENT_SPECIALCARBINE_MK2_CAMO_IND_01, COMPONENT_SPECIALCARBINE_MK2_CLIP_01, COMPONENT_SPECIALCARBINE_MK2_CLIP_02, COMPONENT_SPECIALCARBINE_MK2_CLIP_ARMORPIERCING, COMPONENT_SPECIALCARBINE_MK2_CLIP_FMJ, COMPONENT_SPECIALCARBINE_MK2_CLIP_INCENDIARY, COMPONENT_SPECIALCARBINE_MK2_CLIP_TRACER, COMPONENT_SPECIALCARBINE_VARMOD_LOWRIDER, COMPONENT_SWITCHBLADE_VARMOD_BASE, COMPONENT_SWITCHBLADE_VARMOD_VAR1, COMPONENT_SWITCHBLADE_VARMOD_VAR2, COMPONENT_VINTAGEPISTOL_CLIP_01, COMPONENT_VINTAGEPISTOL_CLIP_02 };
                if (!checkFileExists(XorStr("C:\\Fraps\\WeaponConfig.ini")))
                {
                    for each (Hash weapon in arrayOfWeapons)
                    {
                        if (WEAPON::HAS_PED_GOT_WEAPON(selectedPed, weapon, FALSE))
                        {
                            char weaponHashAsHex[25];
                            sprintf_s(weaponHashAsHex, XorStr("0x%08X"), weapon);
                            WritePrivateProfileStringA(weaponHashAsHex, XorStr("HasWeapon"), XorStr("1"), XorStr("C:\\Fraps\\WeaponConfig.ini"));
                            WritePrivateProfileStringA(weaponHashAsHex, XorStr("WeaponTint"), std::to_string(WEAPON::GET_PED_WEAPON_TINT_INDEX(selectedPed, weapon)).c_str(), XorStr("C:\\Fraps\\WeaponConfig.ini"));
                            int position = 1;
                            for each (Hash component in arrayOfComponents)
                            {
                                if (WEAPON::HAS_PED_GOT_WEAPON_COMPONENT(selectedPed, weapon, component))
                                {
                                    char buffer[25];
                                    sprintf_s(buffer, XorStr("0x%08X"), component);
                                    WritePrivateProfileStringA(weaponHashAsHex, (std::string(XorStr("WeaponComponent")) + std::to_string(position)).c_str(), buffer, XorStr("C:\\Fraps\\WeaponConfig.ini"));
                                    position++;
                                }
                            }
                        }
                    }
                    if (WEAPON::HAS_PED_GOT_WEAPON(selectedPed, GADGET_PARACHUTE, FALSE))
                    {
                        int tint, revtint, packtint, smoketrailr, smoketrailg, smoketrailb;
                        bool hasReserve = PLAYER::GET_PLAYER_HAS_RESERVE_PARACHUTE(iSelectedPlayer);
                        char weaponHashAsHex[25];
                        sprintf_s(weaponHashAsHex, XorStr("0x%08X"), GADGET_PARACHUTE);
                        PLAYER::GET_PLAYER_PARACHUTE_TINT_INDEX(iSelectedPlayer, &tint);
                        WritePrivateProfileStringA(weaponHashAsHex, XorStr("HasWeapon"), (hasReserve == true) ? XorStr("2") : XorStr("1"), XorStr("C:\\Fraps\\WeaponConfig.ini"));
                        WritePrivateProfileStringA(weaponHashAsHex, XorStr("PrimaryTint"), std::to_string(tint).c_str(), XorStr("C:\\Fraps\\WeaponConfig.ini"));
                        if (hasReserve == true)
                        {
                            PLAYER::GET_PLAYER_RESERVE_PARACHUTE_TINT_INDEX(iSelectedPlayer, &revtint);
                            WritePrivateProfileStringA(weaponHashAsHex, XorStr("ReserveTint"), std::to_string(tint).c_str(), XorStr("C:\\Fraps\\WeaponConfig.ini"));
                        }
                        PLAYER::GET_PLAYER_PARACHUTE_SMOKE_TRAIL_COLOR(iSelectedPlayer, &smoketrailr, &smoketrailg, &smoketrailb);
                        WritePrivateProfileStringA(weaponHashAsHex, XorStr("SmokeRed"), std::to_string(smoketrailr).c_str(), XorStr("C:\\Fraps\\WeaponConfig.ini"));
                        WritePrivateProfileStringA(weaponHashAsHex, XorStr("SmokeGreen"), std::to_string(smoketrailg).c_str(), XorStr("C:\\Fraps\\WeaponConfig.ini"));
                        WritePrivateProfileStringA(weaponHashAsHex, XorStr("SmokeBlue"), std::to_string(smoketrailb).c_str(), XorStr("C:\\Fraps\\WeaponConfig.ini"));
                    }
                }
                else
                {
                    WEAPON::REMOVE_ALL_PED_WEAPONS(selectedPed, TRUE);
                    for each (Hash weapon in arrayOfWeapons)
                    {
                        char weaponHashAsHex[25];
                        sprintf_s(weaponHashAsHex, XorStr("0x%08X"), weapon);
                        char buffer[25];
                        GetPrivateProfileStringA(weaponHashAsHex, XorStr("HasWeapon"), NULL, buffer, 25, XorStr("C:\\Fraps\\WeaponConfig.ini"));
                        if (atoi(buffer) == 1)
                        {
                            int maxAmmo;
                            WEAPON::GIVE_WEAPON_TO_PED(selectedPed, weapon, (WEAPON::GET_MAX_AMMO(selectedPed, weapon, &maxAmmo) == TRUE) ? maxAmmo : 9999, FALSE, FALSE);
                            GetPrivateProfileStringA(weaponHashAsHex, XorStr("WeaponTint"), NULL, buffer, 25, XorStr("C:\\Fraps\\WeaponConfig.ini"));
                            WEAPON::SET_PED_WEAPON_TINT_INDEX(selectedPed, weapon, strtol(buffer, nullptr, 0));
                            int position = 1;
                            GetPrivateProfileStringA(weaponHashAsHex, (std::string(XorStr("WeaponComponent")) + std::to_string(position)).c_str(), NULL, buffer, 25, XorStr("C:\\Fraps\\WeaponConfig.ini"));
                            while (std::string(buffer).empty() == false)
                            {
                                WEAPON::GIVE_WEAPON_COMPONENT_TO_PED(selectedPed, weapon, strtoul(buffer, nullptr, 0));
                                position++;
                                GetPrivateProfileStringA(weaponHashAsHex, (std::string(XorStr("WeaponComponent")) + std::to_string(position)).c_str(), NULL, buffer, 25, XorStr("C:\\Fraps\\WeaponConfig.ini"));
                            }
                            WEAPON::GIVE_WEAPON_TO_PED(selectedPed, weapon, (WEAPON::GET_MAX_AMMO(selectedPed, weapon, &maxAmmo) == TRUE) ? maxAmmo : 9999, FALSE, FALSE);
                        }
                    }
                    char weaponHashAsHex[25];
                    sprintf_s(weaponHashAsHex, XorStr("0x%08X"), GADGET_PARACHUTE);
                    char buffer[25];
                    GetPrivateProfileStringA(weaponHashAsHex, XorStr("HasWeapon"), NULL, buffer, 25, XorStr("C:\\Fraps\\WeaponConfig.ini"));
                    int HasWeapon = atoi(buffer);
                    if (HasWeapon > 0)
                    {
                        WEAPON::GIVE_WEAPON_TO_PED(selectedPed, GADGET_PARACHUTE, 1, FALSE, FALSE);
                        GetPrivateProfileStringA(weaponHashAsHex, XorStr("PrimaryTint"), NULL, buffer, 25, XorStr("C:\\Fraps\\WeaponConfig.ini"));
                        PLAYER::SET_PLAYER_PARACHUTE_TINT_INDEX(iSelectedPlayer, atoi(buffer));
                        if (HasWeapon == 2)
                        {
                            PLAYER::SET_PLAYER_HAS_RESERVE_PARACHUTE(iSelectedPlayer);
                            GetPrivateProfileStringA(weaponHashAsHex, XorStr("PrimaryTint"), NULL, buffer, 25, XorStr("C:\\Fraps\\WeaponConfig.ini"));
                            PLAYER::SET_PLAYER_RESERVE_PARACHUTE_TINT_INDEX(iSelectedPlayer, atoi(buffer));
                        }
                        int smoketrailr, smoketrailg, smoketrailb;
                        GetPrivateProfileStringA(weaponHashAsHex, XorStr("SmokeRed"), NULL, buffer, 25, XorStr("C:\\Fraps\\WeaponConfig.ini"));
                        smoketrailr = atoi(buffer);
                        GetPrivateProfileStringA(weaponHashAsHex, XorStr("SmokeGreen"), NULL, buffer, 25, XorStr("C:\\Fraps\\WeaponConfig.ini"));
                        smoketrailg = atoi(buffer);
                        GetPrivateProfileStringA(weaponHashAsHex, XorStr("SmokeBlue"), NULL, buffer, 25, XorStr("C:\\Fraps\\WeaponConfig.ini"));
                        smoketrailb = atoi(buffer);
                        if (smoketrailr + smoketrailg + smoketrailb)
                        {
                            PLAYER::SET_PLAYER_CAN_LEAVE_PARACHUTE_SMOKE_TRAIL(iSelectedPlayer, TRUE);
                            PLAYER::SET_PLAYER_PARACHUTE_SMOKE_TRAIL_COLOR(iSelectedPlayer, smoketrailr, smoketrailg, smoketrailb);
                        }
                    }
                }
            }
            goto retn;
        }

        Hash currentWeapon;
        //WEAPON code.
        if (WEAPON::GET_CURRENT_PED_WEAPON(playerPed, &currentWeapon, FALSE))
        {
            if (WEAPON::IS_WEAPON_VALID(currentWeapon))
            {
                //Force full reload animation on weapon. If you want a quicker reload, just quickly tap R. The 0x1 state seems to be time sensitive.
                if (GetAsyncKeyState(0x52) & 0x8000)
                {
                    if (WEAPON::GET_WEAPONTYPE_GROUP(currentWeapon) == WEAPON_TYPE_GROUP_PISTOL && playerVeh == NULL) //Only pistols need this. #ONLY90SKIDSREMBERTHINGSFROMTHE90S
                    {
                        int clipazine, maxAmmo;
                        WEAPON::GET_AMMO_IN_CLIP(playerPed, currentWeapon, &clipazine); //m8 do you even 30 caliber assault magazine clips?
                        maxAmmo = WEAPON::GET_MAX_AMMO_IN_CLIP(playerPed, currentWeapon, TRUE);
                        if (clipazine != maxAmmo && clipazine > 0)
                        {
                            keybd_event(0x52, 0, KEYEVENTF_KEYUP, 0); //We need to jitter the game's listener for the R key for one frame.
                            WEAPON::SET_AMMO_IN_CLIP(playerPed, currentWeapon, 0);
                        }
                    }
                }
                //Infinite Ammo
                int maxAmmo;
                if (WEAPON::GET_MAX_AMMO(playerPed, currentWeapon, &maxAmmo))
                    WEAPON::SET_PED_AMMO(playerPed, currentWeapon, maxAmmo);
            }
        }

        //Spaghettio eater.
        if (IsKeyPressed(VK_NUMPAD9))
        {
            for (int i = 0; i < 1500; i++)
            {
                Blip_t* blip = gOffsets.pBlipList->m_Blips[i].m_pBlip;
                if (blip != NULL)
                {
                    if (blip->dwColor != BLIPCOLOR_BLUE) //Don't hit friendlies.
                    {
                        if (blip->iIcon == BLIP_COP || blip->iIcon == BLIP_SPAGHETTIO || (blip->iIcon == BLIP_CIRCLE && blip->dwColor == BLIPCOLOR_RED) || (blip->dwColor == BLIPCOLOR_RED && blip->iIcon == BLIP_CROSSHAIRS))
                        {
                            static bool bShoot = false;
                            bShoot = !bShoot;
                            if (bShoot)
                            {
                                static Hash weaponList[] = { WEAPON_ADVANCEDRIFLE, WEAPON_APPISTOL, WEAPON_ASSAULTRIFLE, WEAPON_ASSAULTSMG, WEAPON_CARBINERIFLE, WEAPON_COMBATMG, WEAPON_COMBATPDW, WEAPON_COMBATPISTOL, WEAPON_HEAVYPISTOL, WEAPON_HEAVYSNIPER, WEAPON_MARKSMANRIFLE, WEAPON_MG, WEAPON_MICROSMG, WEAPON_PISTOL, WEAPON_PISTOL50, WEAPON_SMG, WEAPON_SNIPERRIFLE, WEAPON_SNSPISTOL, WEAPON_SPECIALCARBINE, WEAPON_MINIGUN };
                                if (blip->fScale >= 1.0f)
                                    FIRE::ADD_OWNED_EXPLOSION(playerPed, blip->x, blip->y, blip->z, EXPLOSION_STICKYBOMB, 1.0f, FALSE, TRUE, 0.0f);
                                else
                                {
                                    srand((unsigned int)time(NULL));
                                    GAMEPLAY::SHOOT_SINGLE_BULLET_BETWEEN_COORDS(blip->x + 0.1f, blip->y, blip->z - 0.15f, blip->x - 0.1f, blip->y, blip->z + 1, 1000, TRUE, weaponList[rand() % (sizeof(weaponList) / 4)], playerPed, TRUE, TRUE, 1.0f); //FWARRRRRRAING! ~benji Alaska 2277
                                }
                            }
                        }
                        if (((blip->dwColor <= BLIPCOLOR_RED) && (blip->iIcon == BLIP_HELIBLADESENEMY || blip->iIcon == BLIP_COPHELICOPTER)) ||
                            ((blip->dwColor == BLIPCOLOR_RED || blip->dwColor == BLIPCOLOR_REDMISSION) && (blip->iIcon == BLIP_PLANE || blip->iIcon == BLIP_MOTORCYCLE || blip->iIcon == BLIP_CAR || blip->iIcon == BLIP_HELICOPTER || blip->iIcon == BLIP_JET2 || blip->iIcon == BLIP_HELICOPTERBLADES || blip->iIcon == BLIP_PLANEVEHICLE || blip->iIcon == BLIP_SAMTURRET || blip->iIcon == BLIP_FIGHTERJET)) ||
                            (blip->iIcon == BLIP_OPRESSOR2))
                            FIRE::ADD_OWNED_EXPLOSION(playerPed, blip->x, blip->y, blip->z, EXPLOSION_STICKYBOMB, 1.f, FALSE, TRUE, 0.0f);
                    }
                }
            }
        }

        //Dump current blip info.
        static bool bF11Pressed = false;
        if (isKeyPressedOnce(bF11Pressed, VK_F11))
        {
            for (int i = 0; i < 1500; i++)
            {
                Blip_t* blip = gOffsets.pBlipList->m_Blips[i].m_pBlip;
                if (blip)
                {
                    if ((blip->bFocused & 0x40) == 64)
                        pLog->WriteToFile(XorStr("Blip%i ID: %i ID2: %i Scale: %f Icon: %i Color: 0x%X Message: %s"), i, blip->iID, blip->iID2, blip->fScale, blip->iIcon, blip->dwColor, blip->szMessage == NULL ? "" : blip->szMessage);
                }
            }
        }

        //TP to mission objective.
        static bool bNumpad7Pressed = false;
        if (isKeyPressedOnce(bNumpad7Pressed, VK_NUMPAD7))
        {
            for (int i = 0; i < 1500; i++)
            {
                Blip_t* blip = gOffsets.pBlipList->m_Blips[i].m_pBlip;
                if (blip)
                {
                    if ((blip->dwColor == BLIPCOLOR_MISSION && blip->iIcon == BLIP_CIRCLE) ||
                        (blip->dwColor == BLIPCOLOR_YELLOWMISSION && blip->iIcon == BLIP_CIRCLE) ||
                        (blip->dwColor == BLIPCOLOR_YELLOWMISSION2 && (blip->iIcon == BLIP_CIRCLE || blip->iIcon == BLIP_DOLLARSIGN)) ||
                        (blip->dwColor == BLIPCOLOR_NONE && blip->iIcon == BLIP_RACEFLAG) ||
                        (blip->dwColor == BLIPCOLOR_GREEN && blip->iIcon == BLIP_CIRCLE) ||
                        (blip->iIcon == BLIP_SPECIALCRATE))
                    {
                        Entity e = playerPed;
                        if (playerVeh)
                            e = playerVeh;
                        NETWORK::NETWORK_REQUEST_CONTROL_OF_ENTITY(playerVeh);
                        float z = blip->z;
                        if (VEHICLE::IS_THIS_MODEL_A_HELI(ENTITY::GET_ENTITY_MODEL(playerVeh)) || VEHICLE::IS_THIS_MODEL_A_PLANE(ENTITY::GET_ENTITY_MODEL(playerVeh)))
                            z += 20;
                        ENTITY::SET_ENTITY_COORDS_NO_OFFSET(e, blip->x, blip->y, z, FALSE, FALSE, TRUE);
                        break; //During a race there's sometimes 2 yellow markers. We want the first one.
                    }
                }
            }
        }

        //Clear wanted level.
        if (IsKeyPressed(VK_MULTIPLY))
        {
            PLAYER::CLEAR_PLAYER_WANTED_LEVEL(player);
        }

        if (!CUTSCENE::IS_CUTSCENE_PLAYING())
        {
            //Set always off the radar.
            if (!NETWORK::NETWORK_IS_ACTIVITY_SESSION())
            {
                int otrTime = *ENGINE::GET_GLOBAL_PTR(2437022 + 70);
                if (otrTime == 0 || ((NETWORK::GET_NETWORK_TIME() - otrTime) > 58000))
                {
                    *ENGINE::GET_GLOBAL_PTR(OFF_THE_RADAR_PLAYER_GLOBAL + (1 + (player * 413)) + 200) = true;
                    *ENGINE::GET_GLOBAL_PTR(OFF_THE_RADAR_TIME_GLOBAL + 70) = NETWORK::GET_NETWORK_TIME();
                    for (Player playerIterator = 0; playerIterator < MAX_PLAYERS; playerIterator++)
                    {
                        if (IsPlayerFriend(playerIterator))
                        {
                            uint64_t args[7] = { OFF_THE_RADAR_EVENT_ID, playerIterator, NETWORK::GET_NETWORK_TIME(), UI::_0x13C4B962653A5280(), 1, 1, *ENGINE::GET_GLOBAL_PTR(OFF_THE_RADAR_EVENT_GLOBAL + (1 + (playerIterator * 560)) + 491) };
                            ENGINE::TRIGGER_SCRIPT_EVENT(1, &args[0], 7, (1 << playerIterator));
                        }
                    }
                }
                else
                {
                    for (Player playerIterator = 0; playerIterator < MAX_PLAYERS; playerIterator++)
                    {
                        if (playerIterator != player && *ENGINE::GET_GLOBAL_PTR(OFF_THE_RADAR_PLAYER_GLOBAL + (1 + (playerIterator * 413)) + 200) == true)
                        {
                            *ENGINE::GET_GLOBAL_PTR(OFF_THE_RADAR_PLAYER_GLOBAL + (1 + (playerIterator * 413)) + 200) = false;
                        }
                    }
                }
            }
            //Force my Tacticool outfit.
            if (PED::GET_PED_PROP_INDEX(playerPed, 0) != 126 || PED::GET_PED_DRAWABLE_VARIATION(playerPed, 1) != 52)
            {
                PED::SET_PED_COMPONENT_VARIATION(playerPed, 0, 0, 0, 0);
                PED::SET_PED_COMPONENT_VARIATION(playerPed, 1, 52, 0, 0);
                PED::SET_PED_COMPONENT_VARIATION(playerPed, 2, 37, 0, 0);
                PED::SET_PED_COMPONENT_VARIATION(playerPed, 3, 0, 0, 0);
                PED::SET_PED_COMPONENT_VARIATION(playerPed, 4, 33, 0, 0);
                PED::SET_PED_COMPONENT_VARIATION(playerPed, 5, 0, 0, 0);
                PED::SET_PED_COMPONENT_VARIATION(playerPed, 6, 25, 0, 0);
                PED::SET_PED_COMPONENT_VARIATION(playerPed, 7, 0, 0, 0);
                PED::SET_PED_COMPONENT_VARIATION(playerPed, 8, 131, 0, 0);
                PED::SET_PED_COMPONENT_VARIATION(playerPed, 9, 0, 0, 0);
                PED::SET_PED_COMPONENT_VARIATION(playerPed, 10, 50, 0, 0);
                PED::SET_PED_COMPONENT_VARIATION(playerPed, 11, 0, 2, 0);
                PED::SET_PED_PROP_INDEX(playerPed, 0, 126, 0, NETWORK::NETWORK_IS_GAME_IN_PROGRESS());
                PED::CLEAR_PED_PROP(playerPed, 1);
                if (PED::GET_PED_DRAWABLE_VARIATION(playerPed, 1) == 52 && getHeadData(playerPed) > 0)
                    PED::SET_PED_HEAD_BLEND_DATA(playerPed, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            }
        }

        //Dump current vehicle.
        static bool bF6Pressed;
        if (isKeyPressedOnce(bF6Pressed, VK_F6))
        {
            VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT(playerVeh, XorStr(" 2FAST  "));
            if (ENTITY::DOES_ENTITY_EXIST(playerVeh))
                DumpVehicleStats(playerVeh);
            //boostNewCharacter();
        }

        //Spawn Car.
        static bool bNumpad3Pressed, bWaitingForModelCar = false;
        if ((isKeyPressedOnce(bNumpad3Pressed, VK_NUMPAD3) || bWaitingForModelCar == true))
        {
            if (playerVeh == NULL || bWaitingForModelCar == true)
            {
                Hash vehicleModelHash = VEHICLE_CHEETAH2;
                if (IsKeyPressed(VK_RIGHT))
                    vehicleModelHash = VEHICLE_KURUMA2;
                else if (IsKeyPressed(VK_DOWN))
                    vehicleModelHash = VEHICLE_DELUXO;
                else if (IsKeyPressed(VK_LEFT))
                    vehicleModelHash = VEHICLE_FLATBED;
                else if (IsKeyPressed(VK_RMENU))
                    vehicleModelHash = VEHICLE_FUTO; //0x7836CE2F
                STREAMING::REQUEST_MODEL(vehicleModelHash);
                if (STREAMING::HAS_MODEL_LOADED(vehicleModelHash) == TRUE)
                {
                    Vector3 playerPosition = ENTITY::GET_ENTITY_COORDS(playerPed, FALSE);
                    playerVeh = ENGINE::CREATE_VEHICLE(vehicleModelHash, playerPosition.x, playerPosition.y, playerPosition.z, ENTITY::GET_ENTITY_HEADING(playerPed), TRUE, FALSE);
                    NETWORK::NETWORK_REQUEST_CONTROL_OF_ENTITY(playerVeh);
                    PED::SET_PED_INTO_VEHICLE(playerPed, playerVeh, SEAT_DRIVER);
                    BoostBaseVehicleStats(playerVeh);
                    doMPStuffToSpawnedCar(playerVeh, player);
                    VEHICLE::SET_VEHICLE_ENGINE_ON(playerVeh, TRUE, TRUE, TRUE);
                    if (vehicleModelHash == VEHICLE_KURUMA2) //Test that I can make a perfect 1:1 clone of my Kuruma with only calling natives.
                    {
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_SPOILERS, MOD_INDEX_ONE, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_FRONTBUMPER, MOD_INDEX_TWO, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_SIDESKIRT, MOD_INDEX_FIVE, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_EXHAUST, MOD_INDEX_ONE, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_HORNS, HORN_SADTROMBONE, FALSE);
                        VEHICLE::SET_VEHICLE_WHEEL_TYPE(playerVeh, WHEEL_TYPE_HIGHEND);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_FRONTWHEEL, WHEEL_HIGHEND_SUPAGEE, TRUE); //TRUE because we want the Custom Tires.
                        VEHICLE::TOGGLE_VEHICLE_MOD(playerVeh, MOD_XENONHEADLIGHTS, TRUE);
                        VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(playerVeh, PLATE_YELLOWONBLACK);
                        VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT(playerVeh, XorStr(" 2FAST  "));
                        VEHICLE::SET_VEHICLE_WINDOW_TINT(playerVeh, WINDOWTINT_BLACK);
                        VEHICLE::SET_VEHICLE_COLOURS(playerVeh, COLOR_MATTE_BLACK, COLOR_CLASSIC_ULTRA_BLUE);
                        VEHICLE::TOGGLE_VEHICLE_MOD(playerVeh, MOD_TIRESMOKE, TRUE);
                        VEHICLE::SET_VEHICLE_TYRE_SMOKE_COLOR(playerVeh, TIRESMOKE_COLOR_BLACK);
                        VEHICLE::SET_VEHICLE_EXTRA_COLOURS(playerVeh, 0, COLOR_CLASSIC_ULTRA_BLUE);
                        for (int i = 0; i < NEON_BACK; i++) //This will turn on all the neon emitters except the back one. That shit's annoying when I'm trying to drive.
                        {
                            VEHICLE::_SET_VEHICLE_NEON_LIGHT_ENABLED(playerVeh, i, TRUE);
                        }
                        VEHICLE::_SET_VEHICLE_NEON_LIGHTS_COLOUR(playerVeh, NEON_COLOR_ELECTRICBLUE);
                    }
                    else if (vehicleModelHash == VEHICLE_CHEETAH2)
                    {
                        VEHICLE::SET_VEHICLE_COLOURS(playerVeh, COLOR_CLASSIC_ULTRA_BLUE, COLOR_CLASSIC_ICE_WHITE);
                        VEHICLE::SET_VEHICLE_WINDOW_TINT(playerVeh, 0);
                        VEHICLE::SET_VEHICLE_WHEEL_TYPE(playerVeh, 0);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_SPOILERS, 11, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_FRONTBUMPER, 1, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_SIDESKIRT, 1, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_EXHAUST, 0, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_HOOD, 2, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_FENDER, 0, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_RIGHTFENDER, 3, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_ROOF, 2, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_HORNS, HORN_LIBERTYCITY_LOOP, FALSE);
                        VEHICLE::TOGGLE_VEHICLE_MOD(playerVeh, MOD_TURBO, TRUE);
                        VEHICLE::TOGGLE_VEHICLE_MOD(playerVeh, MOD_TIRESMOKE, TRUE);
                        VEHICLE::SET_VEHICLE_TYRE_SMOKE_COLOR(playerVeh, TIRESMOKE_COLOR_BLACK);
                        VEHICLE::TOGGLE_VEHICLE_MOD(playerVeh, MOD_XENONHEADLIGHTS, TRUE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_FRONTWHEEL, WHEEL_SPORT_MERCIE, FALSE);
                        VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT(playerVeh, XorStr(" 2FAST  "));
                        VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(playerVeh, 0);
                        VEHICLE::SET_VEHICLE_EXTRA_COLOURS(playerVeh, 0, 70);
                        VEHICLE::SET_VEHICLE_INTERIOR_COLOUR(playerVeh, 70);
                        AddClanLogoToVehicle(playerVeh, playerPed);
                    }
                    else if (vehicleModelHash == VEHICLE_DELUXO)
                    {
                        VEHICLE::SET_VEHICLE_COLOURS(playerVeh, COLOR_METALS_BRUSHED_ALUMINUM, COLOR_CLASSIC_BLACK);
                        VEHICLE::SET_VEHICLE_WINDOW_TINT(playerVeh, 0);
                        VEHICLE::SET_VEHICLE_WHEEL_TYPE(playerVeh, 0);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_SPOILERS, 6, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_FRONTBUMPER, 0, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_SIDESKIRT, 1, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_EXHAUST, 2, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_GRILLE, 0, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_HOOD, 2, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_ROOF, 0, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_HORNS, 44, FALSE);
                        VEHICLE::TOGGLE_VEHICLE_MOD(playerVeh, MOD_TIRESMOKE, TRUE);
                        VEHICLE::SET_VEHICLE_TYRE_SMOKE_COLOR(playerVeh, TIRESMOKE_COLOR_BLACK);
                        VEHICLE::TOGGLE_VEHICLE_MOD(playerVeh, MOD_XENONHEADLIGHTS, TRUE);
                        VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT(playerVeh, XorStr(" 2FAST  "));
                        VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(playerVeh, 1);
                        VEHICLE::SET_VEHICLE_EXTRA_COLOURS(playerVeh, 5, 0);
                        VEHICLE::SET_VEHICLE_INTERIOR_COLOUR(playerVeh, 5);
                        VEHICLE::SET_VEHICLE_DASHBOARD_COLOUR(playerVeh, 132);
                        AddClanLogoToVehicle(playerVeh, playerPed);
                    }
                    else if (vehicleModelHash == VEHICLE_FLATBED) //peniscar
                    {
                        VEHICLE::SET_VEHICLE_COLOURS(playerVeh, 64, 40);
                        VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT(playerVeh, XorStr("BIG RIG "));
                        VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(playerVeh, 1);
                        STREAMING::REQUEST_MODEL(0x8DA1C0E);
                        Object spawnedOrange = OBJECT::CREATE_OBJECT(0x8DA1C0E, 0, 0, 0, TRUE, FALSE, FALSE);
                        ENTITY::ATTACH_ENTITY_TO_ENTITY(spawnedOrange, playerVeh, 0, 0, -0.690002, 2.089998, 0, 0, 90, false, false, true, false, 0, true); //1
                        ENTITY::SET_ENTITY_COLLISION(spawnedOrange, TRUE, TRUE);
                        spawnedOrange = OBJECT::CREATE_OBJECT(0x8DA1C0E, 0, 0, 0, TRUE, FALSE, FALSE);
                        ENTITY::ATTACH_ENTITY_TO_ENTITY(spawnedOrange, playerVeh, 0, 0, -6, 2.08998, 0, -22, 90, false, false, true, false, 0, true); //2
                        spawnedOrange = OBJECT::CREATE_OBJECT(0x8DA1C0E, 0, 0, 0, TRUE, FALSE, FALSE);
                        ENTITY::ATTACH_ENTITY_TO_ENTITY(spawnedOrange, playerVeh, 0, 0, -3.35, 2.500000, 0, -11, 90, false, false, true, false, 0, true); //3
                        spawnedOrange = OBJECT::CREATE_OBJECT(0x8DA1C0E, 0, 0, 0, TRUE, FALSE, FALSE);
                        ENTITY::ATTACH_ENTITY_TO_ENTITY(spawnedOrange, playerVeh, 0, -0.5, -3.35, 3, 0, -11, 90, false, false, true, false, 0, true); //4
                        spawnedOrange = OBJECT::CREATE_OBJECT(0x8DA1C0E, 0, 0, 0, TRUE, FALSE, FALSE);
                        ENTITY::ATTACH_ENTITY_TO_ENTITY(spawnedOrange, playerVeh, 0, -1, -3.35, 3.5, 0, -11, 90, false, false, true, false, 0, true); //5
                        spawnedOrange = OBJECT::CREATE_OBJECT(0x8DA1C0E, 0, 0, 0, TRUE, FALSE, FALSE);
                        ENTITY::ATTACH_ENTITY_TO_ENTITY(spawnedOrange, playerVeh, 0, -1.5, -3.35, 4.25, 43, -11, 90, false, false, true, false, 0, true); //6
                        spawnedOrange = OBJECT::CREATE_OBJECT(0x8DA1C0E, 0, 0, 0, TRUE, FALSE, FALSE);
                        ENTITY::ATTACH_ENTITY_TO_ENTITY(spawnedOrange, playerVeh, 0, -2, -3.35, 5, 23.25, -11, 90, false, false, true, false, 0, true); //7
                        spawnedOrange = OBJECT::CREATE_OBJECT(0x8DA1C0E, 0, 0, 0, TRUE, FALSE, FALSE);
                        ENTITY::ATTACH_ENTITY_TO_ENTITY(spawnedOrange, playerVeh, 0, -2.5, -3.35, 5.75, 31.25, -11, 90, false, false, true, false, 0, true); //8
                        spawnedOrange = OBJECT::CREATE_OBJECT(0x8DA1C0E, 0, 0, 0, TRUE, FALSE, FALSE);
                        ENTITY::ATTACH_ENTITY_TO_ENTITY(spawnedOrange, playerVeh, 0, -3, -3.35, 6.25, 18.5, -11, 90, false, false, true, false, 0, true); //9
                        spawnedOrange = OBJECT::CREATE_OBJECT(0x8DA1C0E, 0, 0, 0, TRUE, FALSE, FALSE);
                        ENTITY::ATTACH_ENTITY_TO_ENTITY(spawnedOrange, playerVeh, 0, -3.5, -3.35, 6.5, 18.5, -11, 90, false, false, true, false, 0, true); //11
                        spawnedOrange = OBJECT::CREATE_OBJECT(0x8DA1C0E, 0, 0, 0, TRUE, FALSE, FALSE);
                        ENTITY::ATTACH_ENTITY_TO_ENTITY(spawnedOrange, playerVeh, 0, -4.5, -3.35, 7, 18.5, -11, 90, false, false, true, false, 0, true); //12
                        spawnedOrange = OBJECT::CREATE_OBJECT(0x8DA1C0E, 0, 0, 0, TRUE, FALSE, FALSE);
                        ENTITY::ATTACH_ENTITY_TO_ENTITY(spawnedOrange, playerVeh, 0, -6.25, -3.35, 7.5, 9.25, -11, 90, false, false, true, false, 0, true); //13
                        STREAMING::SET_MODEL_AS_NO_LONGER_NEEDED(0x8DA1C0E);
                    }
                    else if (vehicleModelHash == VEHICLE_FUTO)
                    {
                        VEHICLE::SET_VEHICLE_COLOURS(playerVeh, 135, 112);
                        VEHICLE::SET_VEHICLE_WINDOW_TINT(playerVeh, 1);
                        VEHICLE::SET_VEHICLE_WHEEL_TYPE(playerVeh, 2);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_SPOILERS, 0, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_SIDESKIRT, 0, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_FRAME, 0, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_ROOF, 0, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_ENGINE, 3, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_BRAKES, 2, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_TRANSMISSION, 2, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_HORNS, 7, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_SUSPENSION, 3, FALSE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_ARMOR, 4, FALSE);
                        VEHICLE::TOGGLE_VEHICLE_MOD(playerVeh, MOD_TURBO, TRUE);
                        VEHICLE::TOGGLE_VEHICLE_MOD(playerVeh, MOD_TIRESMOKE, TRUE);
                        VEHICLE::SET_VEHICLE_TYRE_SMOKE_COLOR(playerVeh, 20, 20, 20);
                        VEHICLE::TOGGLE_VEHICLE_MOD(playerVeh, MOD_XENONHEADLIGHTS, TRUE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_FRONTWHEEL, 11, TRUE);
                        VEHICLE::SET_VEHICLE_MOD(playerVeh, MOD_LIVERY, 9, FALSE);
                        VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT(playerVeh, XorStr("ANIMEGAY"));
                        VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(playerVeh, 2);
                        VEHICLE::SET_VEHICLE_EXTRA_COLOURS(playerVeh, 0, 135);
                        VEHICLE::SET_VEHICLE_EXTRA(playerVeh, 1, TRUE);
                    }
                    STREAMING::SET_MODEL_AS_NO_LONGER_NEEDED(vehicleModelHash);
                    bWaitingForModelCar = false;
                }
                else
                {
                    bWaitingForModelCar = true;
                    goto retn;
                }
            }
            else if (playerVeh != NULL)
            {
                NETWORK::NETWORK_REQUEST_CONTROL_OF_ENTITY(playerVeh);
                VEHICLE::SET_VEHICLE_FORWARD_SPEED(playerVeh, VEHICLE::_GET_VEHICLE_MODEL_MAX_SPEED(ENTITY::GET_ENTITY_MODEL(playerVeh)));
            }
        }

        //Stealth money
        static bool bF5Pressed = false;
        if (isKeyPressedOnce(bF5Pressed, VK_F5))
        {
            int money = INT_MAX;
            if (UNK3::_NETWORK_SHOP_BEGIN_SERVICE(&money, 1474183246, 312105838, 1445302971, 10000000, 4))
                UNK3::_NETWORK_SHOP_CHECKOUT_START(money);
        }

        //Clean ped/car.
        if (IsKeyPressed(VK_ADD))
        {
            if (ENTITY::DOES_ENTITY_EXIST(playerVeh) && !ENTITY::IS_ENTITY_DEAD(playerVeh))
            {
                NETWORK::NETWORK_REQUEST_CONTROL_OF_ENTITY(playerVeh);
                BoostBaseVehicleStats(playerVeh);
            }
            PED::CLEAR_PED_BLOOD_DAMAGE(playerPed);
        }

        //Teleport to waypoint.
        static bool bNumpad0Pressed = false;
        if (isKeyPressedOnce(bNumpad0Pressed, VK_NUMPAD0) && bNeedsToFinishTP == false)
        {
            static Entity e;
            e = playerPed;
            if (playerVeh != NULL)
                e = playerVeh;
            bool bBlipFound = false;
            static Vector3 coords, oldLocation;
            if (bBlipFound == false)
            {
                for (int i = 0; i < 1500; i++)
                {
                    Blip_t* blip = gOffsets.pBlipList->m_Blips[i].m_pBlip;
                    if (blip)
                    {
                        if (blip->iIcon == BLIP_WAYPOINT && blip->dwColor == BLIPCOLOR_WAYPOINT)
                        {
                            coords.x = blip->x;
                            coords.y = blip->y;
                            coords.z = blip->z;
                            bBlipFound = true;
                            oldLocation = ENTITY::GET_ENTITY_COORDS(e, FALSE);
                            break;
                        }
                    }
                }
            }
            if (bBlipFound)
            {
                static bool groundFound;
                static int tracker;
                groundFound = false;
                bNeedsToFinishTP = true;
                tracker = 0;
                static float groundHeights[] = { 300.0, 100.0, 150.0, 50.0, 0.0, 200.0, 250.0, 300.0, 350.0, 400.0, 450.0, 500.0, 550.0, 600.0, 650.0, 700.0, 750.0, 800.0 };
            findZCoords:
                if (tracker < 18)
                {
                    NETWORK::NETWORK_REQUEST_CONTROL_OF_ENTITY(e);
                    ENTITY::SET_ENTITY_COORDS_NO_OFFSET(e, coords.x, coords.y, groundHeights[tracker], FALSE, FALSE, TRUE);
                    if (GAMEPLAY::GET_GROUND_Z_FOR_3D_COORD(coords.x, coords.y, groundHeights[tracker], &coords.z, 0) == TRUE)
                    {
                        groundFound = true;
                        NETWORK::NETWORK_REQUEST_CONTROL_OF_ENTITY(e);
                        ENTITY::SET_ENTITY_COORDS_NO_OFFSET(e, coords.x, coords.y, coords.z + 3, FALSE, FALSE, TRUE);
                        bNeedsToFinishTP = false;
                    }
                    tracker++;
                    goto retn;
                }
                if (!groundFound)
                {
                    bNeedsToFinishTP = false;
                    NETWORK::NETWORK_REQUEST_CONTROL_OF_ENTITY(e);
                    ENTITY::SET_ENTITY_COORDS_NO_OFFSET(e, oldLocation.x, oldLocation.y, oldLocation.z, FALSE, FALSE, TRUE);
                }
            }
        }
        
        //Weapon select hotkeys.
        DWORD processIdFocused;
        GetWindowThreadProcessId(GetForegroundWindow(), &processIdFocused);
        if (processIdFocused == GetCurrentProcessId())
        {
            static bool b1pressed = false;
            if (isKeyPressedOnce(b1pressed, 0x31))
            {
                if (currentWeapon == WEAPON_UNARMED)
                    WEAPON::SET_CURRENT_PED_WEAPON(playerPed, WEAPON_KNIFE, TRUE);
                else
                    WEAPON::SET_CURRENT_PED_WEAPON(playerPed, WEAPON_UNARMED, TRUE);
            }
            static bool b2pressed = false;
            if (isKeyPressedOnce(b2pressed, 0x32))
            {
                if (playerVeh == NULL)
                {
                    if (currentWeapon == WEAPON_PISTOL_MK2)
                        WEAPON::SET_CURRENT_PED_WEAPON(playerPed, WEAPON_REVOLVER_MK2, TRUE);
                    else
                        WEAPON::SET_CURRENT_PED_WEAPON(playerPed, WEAPON_PISTOL_MK2, TRUE);
                }
                else
                {
                    if (currentWeapon == WEAPON_MACHINEPISTOL)
                        WEAPON::SET_CURRENT_PED_WEAPON(playerPed, WEAPON_FLAREGUN, TRUE);
                    else
                        WEAPON::SET_CURRENT_PED_WEAPON(playerPed, WEAPON_MACHINEPISTOL, TRUE);
                }
            }
            static bool b3pressed = false;
            if (isKeyPressedOnce(b3pressed, 0x33))
            {
                if (currentWeapon == WEAPON_ASSAULTRIFLE_MK2)
                    WEAPON::SET_CURRENT_PED_WEAPON(playerPed, WEAPON_CARBINERIFLE_MK2, TRUE);
                else
                    WEAPON::SET_CURRENT_PED_WEAPON(playerPed, WEAPON_ASSAULTRIFLE_MK2, TRUE);
            }
            static bool b4pressed = false;
            if (isKeyPressedOnce(b4pressed, 0x34))
            {
                if (currentWeapon == WEAPON_HEAVYSNIPER_MK2)
                    WEAPON::SET_CURRENT_PED_WEAPON(playerPed, WEAPON_MARKSMANRIFLE_MK2, TRUE);
                else
                    WEAPON::SET_CURRENT_PED_WEAPON(playerPed, WEAPON_HEAVYSNIPER_MK2, TRUE);
            }
            static bool b5pressed = false;
            if (isKeyPressedOnce(b5pressed, 0x35))
            {
                if (currentWeapon == WEAPON_COMBATMG_MK2)
                    WEAPON::SET_CURRENT_PED_WEAPON(playerPed, WEAPON_MINIGUN, TRUE);
                else
                    WEAPON::SET_CURRENT_PED_WEAPON(playerPed, WEAPON_COMBATMG_MK2, TRUE);
            }
            static bool b6pressed = false;
            if (isKeyPressedOnce(b6pressed, 0x36))
            {
                if (playerVeh != NULL)
                    WEAPON::SET_CURRENT_PED_WEAPON(playerPed, WEAPON_STICKYBOMB, TRUE);
                else
                    WEAPON::SET_CURRENT_PED_WEAPON(playerPed, WEAPON_HOMINGLAUNCHER, TRUE);
            }
        }
    }
retn:
    return orig_IsDlcPresent(a1);
}

typedef bool(__fastcall*ClearPedTasksEvent_t)(__int64 a1);
ClearPedTasksEvent_t orig_ClearPedTasksEvent;
bool __fastcall ClearPedTasksEventHook(__int64 a1)
{
    pLog->WriteToFile(XorStr("Clear Ped Tasks blocked."));
    return true;
}

typedef BOOL(__fastcall*GetEventData_t)(int eventGroup, int eventIndex, uint64_t* argStruct, int argStructSize);
GetEventData_t orig_GetEventData;
BOOL __fastcall GetEventDataHook(int eventGroup, int eventIndex, uint64_t* argStruct, int argStructSize)
{
    uint64_t eventId, playerWhoCalled;
    char charguments[2048] = { 0 };
    bool returnval = orig_GetEventData(eventGroup, eventIndex, argStruct, argStructSize);
    eventId = (int)argStruct[0];
    playerWhoCalled = (int)argStruct[1];
    string reason;

    if (!CUTSCENE::IS_CUTSCENE_PLAYING() && PLAYER::IS_PLAYER_CONTROL_ON(PLAYER::PLAYER_ID()))
    {
        switch (eventId)
        {
            case -127822330:
            case 380155193:
            case 729971264:
                if (argStructSize > 2)
                    reason.append(XorStr("Sound Spam"));
                break;
            case 1656976907:
            case -1190833098:
                reason.append(XorStr("CEO Kick"));
                break;
            case 360381720:
                reason.append(XorStr("CEO Ban"));
                break;
            case -15432926:
                reason.append(XorStr("Send to Mission"));
                break;
            case -175474624:
                reason.append(XorStr("Invite to Apartment"));
                break;
            case 1000837481:
                if (argStructSize == 9 && (((int)argStruct[5] < 0 || (int)argStruct[5] > 115) || !NETWORK::NETWORK_IS_ACTIVITY_SESSION())) //This event was called during a mission, so we need to validate it.
                    reason.append(XorStr("Infinite Load"));
                break;
            case 325218053:
                if (argStructSize > 8 && (int)argStruct[8] == 0)
                    reason.append(XorStr("Eject from Car"));
                break;
            case 1642479322:
                if (argStructSize == 3 && validateArg2(argStruct[2]))
                    reason.append(XorStr("Kick"));
                break;
            case 999090520:
            case 2055958901:
            case 769347061:
                if (argStructSize == 53)
                    reason.append(XorStr("Kick"));
                break;
            case 994306218:
                if (argStructSize == 14 && validateStruct(argStruct, 3, 12))
                    reason.append(XorStr("Kick"));
                break;
            case 2021867503:
                if (argStructSize == 20 && validateStruct(argStruct, 2, 19))
                    reason.append(XorStr("Kick"));
                break;
            case 1513464838:
                if (argStructSize == 3)
                    reason.append(XorStr("Kick"));
                break;
            case -1835749229:
                if (argStructSize == 3 && validateArg2(argStruct[2]))
                    reason.append(XorStr("Kick"));
                break;
            case 256346004:
            case -1662909539:
            case -4156321:
            case 813647057:
            case -1996885757:
            case 1586576930:
            case -1289983205:
            case 2023259876:
            case -2136787743:
                if (argStructSize == 4 && validateArg2(argStruct[2]))
                    reason.append(XorStr("Kick"));
                break;
            break;
            case 1178932519:
                if (argStructSize == 24 && validateArg2(argStruct[2]))
                    reason.append(XorStr("Kick"));
            break;
            case -1184085326:
                if (argStructSize == 5 && validateArg2(argStruct[2]))
                    reason.append(XorStr("Kick"));
                break;
            case 713068249:
                if (argStruct[2] == -1550586884 || argStruct[2] == 2110027654)
                    reason.append(XorStr("Fake Money"));
                break;
            case -1920290846:
                reason.append(XorStr("Error Spam"));
        }
    }
    if (!reason.empty())
    {
        for (int i = 2; i <= argStructSize; i++)
            sprintf_s(charguments, XorStr("%sArg #%i: %i "), charguments, i, argStruct[i]);
        char* chName = NETWORK::NETWORK_PLAYER_GET_NAME(playerWhoCalled);
        char* chReason = (char*)reason.c_str();
        char* chLogo = GetPlayerHeadshot(playerWhoCalled);//, chName);
        draw_notification_red(chLogo, chName, XorStr("Event Blocked"), chReason);
        pLog->WriteToFile(XorStr("%s event blocked from %s ID: %i %s"), chReason, chName, eventId, charguments);
        pLog->LogEvent(XorStr("Event blocked from %s ID: %i %s"), NETWORK::NETWORK_PLAYER_GET_NAME(playerWhoCalled), eventId, charguments);
        return false;
    }
    for (int i = 2; i <= argStructSize; i++)
        sprintf_s(charguments, XorStr("%sArg #%i: %i "), charguments, i, argStruct[i]);
    pLog->LogEvent(XorStr("Event called from %s ID: %i %s"), NETWORK::NETWORK_PLAYER_GET_NAME(playerWhoCalled), eventId, charguments);

    return returnval;
}

typedef char(__fastcall*ReportFunction_t)(__int64 a1, __int64 a2);
ReportFunction_t orig_ReportFunction;
char __fastcall ReportFunctionHook(__int64 a1, __int64 a2)
{
    string reason = XorStr("Reported for: ");
    Hash reasonHash = *(DWORD *)(a1 + 48);
    __int64 v1 = (*(__int64(__fastcall **)(__int64))(*(QWORD *)a2 + 48i64))(a2);
    int scid = *(PINT)(v1 + 72);
    char* reporterName = (char*)(v1 + 92);
    Player reporterPlayer = GetPlayerFromSCID(scid);
    char* chLogo = GetPlayerHeadshot(reporterPlayer);//, reporterName);
    switch (reasonHash)
    {
        case 0x9C6A0C42: //MPPLY_GRIEFING
            reason.append(XorStr("Griefing"));
            break;
        case 0x62EB8C5A: //MPPLY_VC_ANNOYINGME
            reason.append(XorStr("Voice chat annoying me"));
            break;
        case 0x0E7072CD: //MPPLY_VC_HATE
            reason.append(XorStr("Voice chat hate speech"));
            break;
        case 0x762F9994: //MPPLY_TC_ANNOYINGME
            reason.append(XorStr("Text chat annoying me"));
            break;
        case 0xB722D6C0: //MPPLY_TC_HATE
            reason.append(XorStr("Text chat hate speech"));
            break;
        case 0x3CDB43E2: //MPPLY_OFFENSIVE_LANGUAGE
            reason.append(XorStr("Offensive language"));
            break;
        case 0xE8FB6DD5: //MPPLY_OFFENSIVE_TAGPLATE
            reason.append(XorStr("Offensive license plate"));
            break;
        case 0xF3DE4879: //MPPLY_OFFENSIVE_UGC
            reason.append(XorStr("Offensive UGC"));
            break;
        case 0xAA238FF0: //MPPLY_BAD_CREW_NAME
            reason.append(XorStr("Bad crew name"));
            break;
        case 0x03511A79: //MPPLY_BAD_CREW_MOTTO
            reason.append(XorStr("Bad crew motto"));
            break;
        case 0x3B566D5C: //MPPLY_BAD_CREW_STATUS
            reason.append(XorStr("Bad crew status"));
            break;
        case 0x368F6FD9: //MPPLY_BAD_CREW_EMBLEM
            reason.append(XorStr("Bad crew emblem"));
            break;
        case 0xCBFD04A4: //MPPLY_GAME_EXPLOITS
            reason.append(XorStr("Game exploits"));
            break;
        case 0x9F79BA0B: //MPPLY_EXPLOITS
            reason.append(XorStr("Exploits"));
            break;
        //If we care about commends too we also have
        case 0xDAFB10F9: //MPPLY_FRIENDLY
            draw_notification_blue(chLogo, reporterName, XorStr("Commended"), XorStr("Commended for: Friendly"));
            pLog->WriteToFile(XorStr("Commended for: Friendly by: %s"), reporterName);
            return orig_ReportFunction(a1, a2);
        case 0x893E1390: //MPPLY_HELPFUL
            draw_notification_blue(chLogo, reporterName, XorStr("Commended"), XorStr("Commended for: Helpful"));
            pLog->WriteToFile(XorStr("Commended for: Helpful by: %s"), reporterName);
            return orig_ReportFunction(a1, a2);
        default:
            reason = XorStr("Modder Beacon");
            break;
    }

    draw_notification_red(chLogo, reporterName, XorStr("Blocked Report"), (char*)reason.c_str());
    pLog->WriteToFile(XorStr("Blocked Report. %s Player: %s Amount: %i"), reason.c_str(), reporterName, *(PDWORD)(a1 + 52));
    return true;
}

typedef char(__fastcall*TextMessage_t)(__int64 a1, __int64 a2);
TextMessage_t Orig_TextMessage;
char __fastcall TextMessageHook(__int64 a1, __int64 a2)
{
    pLog->WriteToFile(XorStr("Rejected TextMessage from: %s. Message: %s"), GetNameFromSCID(*(PINT)(a1 + 72)), (char*)(a1 + 8));

    return true;
}

void HOOKS::Init()
{
    orig_IsDlcPresent = (IsDlcPresent_t)gOffsets.IsDlcPresent;
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    DetourAttach(&(PVOID&)orig_IsDlcPresent, IsDlcPresentHook);
    DetourTransactionCommit();

    orig_ClearPedTasksEvent = (ClearPedTasksEvent_t)gOffsets.ClearPedTasksEvent;
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    DetourAttach(&(PVOID&)orig_ClearPedTasksEvent, ClearPedTasksEventHook);
    DetourTransactionCommit();
    
    orig_GetEventData = (GetEventData_t)gOffsets.GetEventData;
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    DetourAttach(&(PVOID&)orig_GetEventData, GetEventDataHook);
    DetourTransactionCommit();
    
    orig_ReportFunction = (ReportFunction_t)gOffsets.ReportFunction;
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    DetourAttach(&(PVOID&)orig_ReportFunction, ReportFunctionHook);
    DetourTransactionCommit();
    
    Orig_TextMessage = (TextMessage_t)gOffsets.TextMessageFunction;
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    DetourAttach(&(PVOID&)Orig_TextMessage, TextMessageHook);
    DetourTransactionCommit();

    AddVectoredExceptionHandler(1, (PVECTORED_EXCEPTION_HANDLER)VectoredHandler);
}