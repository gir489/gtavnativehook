#pragma once
using namespace std;

class Log
{
public:
    Log(char* filename);
    ~Log();
    char *GetDirectoryFile(char *filename);
    void WriteToFile(LPCSTR fmt, ...);
    void LogEvent(LPCSTR fmt, ...);
private:
    ofstream m_stream;
};

extern Log* pLog;
extern bool bDebug;


