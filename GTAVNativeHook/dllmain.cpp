#include "dllmain.h"

DWORD dwImageSize = NULL;
DWORD64 dwImageBase = NULL;

COffsets gOffsets;

BOOL APIENTRY DllMain(HINSTANCE hModule, DWORD dwReason, LPVOID lpReserved)
{
    if (dwReason == DLL_PROCESS_ATTACH)
    {
        dwImageBase = (DWORD64)gOffsets.GetModuleBaseAddress(gOffsets.GetWC(XorStr("GTA5.exe")));

        PIMAGE_NT_HEADERS nthdr = gOffsets.GetNTHeader(gOffsets.GetModuleBaseAddress(gOffsets.GetWC(XorStr("GTA5.exe"))));
        if (nthdr == nullptr)
            return false;

        dwImageSize = nthdr->OptionalHeader.SizeOfImage;

        pLog->WriteToFile(XorStr("ModuleBase %p | ModuleSize: %X"), dwImageBase, dwImageSize);

        if (dwImageBase != NULL && dwImageSize != NULL)
        {
            gOffsets.Init();
        }
    }

    return TRUE;
}