#pragma once

class CObject
{
public:
    char pad_0x0000[0x8]; //0x0000

}; //Size=0x0008

class CObjectHandle
{
public:
    CObject * pCObject; //0x0000
    __int32 iHandle; //0x0008
    char pad_0x000C[0x4]; //0x000C

}; //Size=0x0010

class CObjectList
{
public:
    CObjectHandle objs[2300]; //0x0000

}; //Size=0x8FC0

class CObjectInterface
{
public:
    char pad_0x0000[0x158]; //0x0000
    CObjectList* pCObjectList; //0x0158
    __int32 iMaxObjects; //0x0160
    char pad_0x0164[0x4]; //0x0164
    __int32 iCurObjects; //0x0168
    char pad_0x016C[0x5C]; //0x016C

    CObject* get_object(const int& index)
    {
        if (index < iMaxObjects)
            return pCObjectList->objs[index].pCObject;
        return nullptr;
    }

}; //Size=0x01C8   

class CPed
{
public:
    char pad_0x0000[0x28]; //0x0000
    BYTE btEntityType; //0x0028 
    char pad_0x0029[0x3]; //0x0029
    BYTE btInvisible; //0x002C 
    char pad_0x002D[0x1]; //0x002D
    BYTE btFreezeMomentum; //0x002E 
    char pad_0x002F[0x1]; //0x002F
    void* pCNavigation; //0x0030 
    char pax_0x0038[0x10]; //0x0038
    void*	pCPedStyle; //0x0048
    char pad_0x0038[0x40]; //0x0050
    void* v3VisualPos; //0x0090 
    char pad_0x009C[0xED]; //0x009C
    BYTE btGodMode; //0x0189 
    char pad_0x018A[0xF6]; //0x018A
    float fHealth; //0x0280 
    char pad_0x0284[0x1C]; //0x0284
    float fHealthMax; //0x02A0 
    char pad_0x02A4[0x4]; //0x02A4
    void* pCAttacker; //0x02A8 
    char pad_0x02B0[0x70]; //0x02B0
    void* v3Velocity; //0x0320 
    char pad_0x032C[0x9FC]; //0x032C
    void* pCVehicleLast; //0x0D28 
    char pad_0x0D30[0x378]; //0x0D30
    BYTE btNoRagdoll; //0x10A8 
    char pad_0x10A9[0xF]; //0x10A9
    void* pCPlayerInfo; //0x10B8 
    char pad_0x10C0[0x8]; //0x10C0
    void* pCWeaponManager; //0x10C8 
    char pad_0x10D0[0x31C]; //0x10D0
    BYTE btSeatBelt; //0x13EC 
    char pad_0x13ED[0xB]; //0x13ED
    BYTE btSeatbeltWindshield; //0x13F8 
    char pad_0x13F9[0x1]; //0x13F9
    BYTE btCanSwitchWeapons;
    char pad_0x13FB[0x5];
    BYTE btForcedAim; //0x1400 
    BYTE N00000936; //0x1401 
    BYTE N00000939; //0x1402 
    BYTE N00000937; //0x1403 
    char pad_0x1404[0x67]; //0x1404
    BYTE btIsInVehicle; //0x146B 
    char pad_0x146C[0x44]; //0x146C
    float fArmor; //0x14B0 
    char pad_0x14B4[0x20]; //0x14B4
    float fFatiguedHealthThreshold; //0x14D4 
    float fInjuredHealthThreshold; //0x14D8 
    float fDyingHealthThreshold; //0x14DC 
    float fHurtHealthThreshold; //0x14E0 
    char pad_0x14E4[0xC]; //0x14E4
    void* pCVehicleLast2; //0x14F0 
    char pad_0x14F8[0xDC]; //0x14F8
    __int32 iCash; //0x15D4 

//entity type 3 = car, 4 = ped
    bool isGod()
    {
        return((btGodMode & 0x01)
            //||	fFatiguedHealthThreshold < 0.f
            //||	fInjuredHealthThreshold < 0.f
            //||	fDyingHealthThreshold < 0.f
            || fHurtHealthThreshold < 0.f);
    }

    bool isInvisible()
    {
        return (btInvisible & 0x01) ? true : false;
    }

    bool hasFrozenMomentum()
    {
        return (btFreezeMomentum & 0x02) ? true : false;
    }

    bool canBeRagdolled()
    {
        return (btNoRagdoll & 0x20) ? false : true;
    }

    bool hasSeatbelt()
    {
        return (btSeatBelt & 0x01) ? true : false;
    }

    bool hasSeatbeltWindshield()
    {
        return (btSeatbeltWindshield & 0x10) ? true : false;
    }

    bool isInVehicle()
    {
        return (btIsInVehicle & 0x10) ? false : true;
    }

    void giveHealth()
    {
        if (fHealth < fHealthMax)
            fHealth = fHealthMax;
        if (fArmor < 50.f)
            fArmor = 50.f;
    }

    void giveHealth(float value)
    {
        value += fHealth;
        if (value <= fHealthMax)
            fHealth = value;
        else if (fHealth < fHealthMax)
            fHealth = fHealthMax;
    }

    void setForcedAim(bool toggle)
    {
        btForcedAim ^= (btForcedAim ^ -(char)toggle) & 0x20;
    }
}; //Size=0x15D8

class CPedHandle
{
public:
    CPed* pCPed; //0x0000 
    __int32 iHandle;
    char _pad0[0x4];

}; //Size=0x0010

class CPedList
{
public:
    CPedHandle peds[256]; //0x0000 

}; //Size=0x1000

class CPedInterface
{
public:
    char pad_0x0000[0x100]; //0x0000
    CPedList* PedList; //0x0100 
    __int32 iMaxPeds; //0x0108 
    char pad_0x010C[0x4]; //0x010C
    __int32 iCurPeds; //0x0110 
    char pad_0x0114[0x34]; //0x0114

    CPed* get_ped(const int& index)
    {
        if (index < iMaxPeds)
            return PedList->peds[index].pCPed;
        return nullptr;
    }
}; //Size=0x0148

class CReplayInterface
{
public:
    void* N000006F5; //0x0000
    void* pCCameraInterface; //0x0008
    void* pCVehicleInterface; //0x0010
    CPedInterface* pCPedInterface; //0x0018 
    void* pCPickupInterface; //0x0020
    CObjectInterface* pCObjectInterface; //0x0028

}; //Size=0x0030