#include "dllmain.h"

#pragma region ENGINE
bool ENGINE::STAT_SET_INT(Hash statName, int value, BOOL save)
{
    typedef bool(__thiscall* function_t)(Hash statName, int value, BOOL save);
    static function_t originalFunction = (function_t)((DWORD64)gOffsets.STAT_SET_INT);

    return originalFunction(statName, value, save);
}
bool ENGINE::STAT_SET_FLOAT(Hash statName, float value, BOOL save)
{
    typedef bool(__thiscall* function_t)(Hash statName, float value, BOOL save);
    static function_t originalFunction = (function_t)((DWORD64)gOffsets.STAT_SET_FLOAT);

    return originalFunction(statName, value, save);
}
bool ENGINE::STAT_SET_BOOL(Hash statName, bool value, BOOL save)
{
    typedef bool(__thiscall* function_t)(Hash statName, bool value, BOOL save);
    static function_t originalFunction = (function_t)((DWORD64)gOffsets.STAT_SET_BOOL);

    return originalFunction(statName, value, save);
}
void ENGINE::TRIGGER_SCRIPT_EVENT(int eventGroup, uint64_t* args, int argCount, int bitset)
{
    typedef void(__thiscall* function_t)(int eventGroup, uint64_t* args, int argCount, int bitset);
    static function_t originalFunction = (function_t)((DWORD64)gOffsets.TRIGGER_SCRIPT_EVENT);

    originalFunction(eventGroup, args, argCount, bitset);
}
__int64* ENGINE::GET_GLOBAL_PTR(int index)
{
    return &gOffsets.GlobalBase[index >> 0x12 & 0x3F][index & 0x3FFFF];
}

Vehicle ENGINE::CREATE_VEHICLE(Hash modelHash, float x, float y, float z, float heading, BOOL isNetwork, BOOL thisScriptCheck)
{
    *(unsigned short*)(gOffsets.dwGetModelTableFunctionAddress + 0x8) = 0x9090;
    Vehicle retVar = VEHICLE::CREATE_VEHICLE(modelHash, x, y, z, heading, isNetwork, thisScriptCheck);
    *(unsigned short*)(gOffsets.dwGetModelTableFunctionAddress + 0x8) = 0x0574;
    return retVar;
}

#pragma endregion

NativeHandler GetNativeHandler(UINT64 hash)
{
    NativeRegistration** registrationTable = (NativeRegistration**)gOffsets.RegistrationTable;
    NativeRegistration* table = registrationTable[hash & 0xFF];
    for (; table; table = table->getNextRegistration())
    {
        for (uint32_t i = 0; i < table->getNumEntries(); i++)
        {
            if (hash == table->getHash(i))
            {
                return table->handlers[i];
            }
        }
    }

    return nullptr;
}

void NativeInvoke::Invoke(NativeContext *cxt, UINT64 hash)
{
    try
    {
        auto fn = GetNativeHandler(hash);
        if (fn != 0)
        {
            //pLog->WriteToFile(XorStr("Hash: 0x%p Function: GTA5.exe+%X"), hash, (unsigned long long)fn - (unsigned long long)dwImageBase);
            fn(cxt);
        }
        else
        {
            char arguments[2048] = { 0 };
            for (int i = 0; i < cxt->GetArgumentCount(); i++)
            {
                sprintf_s(arguments, XorStr("%sArg #%i: %p "), arguments, i, cxt->GetArgument<QWORD>(i));
            }
            pLog->WriteToFile(XorStr("Failed to find Native: 0x%p %s"), hash, arguments);

        }
    }
    catch (...)
    {
        char arguments[2048] = { 0 };
        for (int i = 0; i < cxt->GetArgumentCount(); i++)
        {
            sprintf_s(arguments, XorStr("%sArg #%i: %p "), arguments, i, cxt->GetArgument<QWORD>(i));
        }
        pLog->WriteToFile(XorStr("Failed to Invoke: 0x%p %s"), hash, arguments);
    }
}
#pragma endregion