#pragma once

#define SCORE_BOARD_HEADSHOT_GLOBAL 1379953 + 2
#define OFF_THE_RADAR_PLAYER_GLOBAL 2423801
#define OFF_THE_RADAR_TIME_GLOBAL 2437022
#define OFF_THE_RADAR_EVENT_GLOBAL 1625435
#define OFF_THE_RADAR_EVENT_ID 1344161996
#define BUNKER_RESEARCH_GLOBAL 262145 + 21246

template< typename Ty > Ty make_ptr(void* ptr, DWORD_PTR offset)
{
    return reinterpret_cast<Ty>(reinterpret_cast<DWORD_PTR> (ptr) + offset);
}

struct GetPatternByte
{
    GetPatternByte() : ignore(true)
    {
    }

    GetPatternByte(std::string byteString, bool ignoreThis = false) {
        data = StringToUint8(byteString);
        ignore = ignoreThis;
    }

    bool ignore;
    UINT8 data;

private:
    UINT8 StringToUint8(std::string str)
    {
        std::istringstream iss(str);

        UINT32 ret;

        if (iss >> std::hex >> ret)
        {
            return (UINT8)ret;
        }

        return 0;
    }
};

static struct FindPattern
{
    static DWORD64 Find(DWORD64 dwStart, DWORD64 dwLength, std::string s) {
        std::vector<GetPatternByte> p;
        std::istringstream iss(s);
        std::string w;

        while (iss >> w)
        {
            if (w.data()[0] == '?')
            {
                p.push_back(GetPatternByte());
            }
            else if (w.length() == 2 && isxdigit(w.data()[0]) && isxdigit(w.data()[1])) { // Hex
                p.push_back(GetPatternByte(w));
            }
            else
            {
                return NULL;
            }
        }

        for (DWORD64 i = 0; i < dwLength; i++)
        {
            UINT8* lpCurrentByte = (UINT8*)(dwStart + i);

            bool found = true;

            for (size_t ps = 0; ps < p.size(); ps++)
            {
                if (p[ps].ignore == false && lpCurrentByte[ps] != p[ps].data) {
                    found = false;
                    break;
                }
            }

            if (found)
            {
                return (DWORD64)lpCurrentByte;
            }
        }

        return NULL;
    }

    static DWORD64 Find(std::string s)
    {
        auto value = Find((DWORD64)dwImageBase, (DWORD64)dwImageSize, s);
        if (value == NULL)
        {
            pLog->WriteToFile(XorStr("Failed to find signature for: %s"), s.c_str());
            exit(0);
        }
        return value;
    }
};

class COffsets
{
public:
    DWORD64 IsDlcPresent, RegistrationTable, ClearPedTasksEvent, GetEventData, ReportFunction, KickFromSession, TextMessageFunction, dwAddOwnedExplosionBypass, ptrToHandleAddress, ModelCrashLocation, dwGetModelTableFunctionAddress = 0L;
    DWORD64 ADD_OWNED_EXPLOSION, STAT_SET_BOOL, STAT_SET_FLOAT, STAT_SET_INT, TRIGGER_SCRIPT_EVENT = 0L;
    CReplayInterface* replayInterface = nullptr;
    __int64** GlobalBase;
    
    BlipList* pBlipList;

    void Init();
    HMODULE __stdcall GetModuleBaseAddress(LPCWSTR moduleName);
    PIMAGE_NT_HEADERS GetNTHeader(HMODULE hmModule);
    const wchar_t * GetWC(const char * c);
};

extern COffsets gOffsets;