#pragma once

constexpr auto MAX_PLAYERS = 32;

static void CheckPlayer(int& iPlayer, bool direction)
{
    int iOriginalPlayer = iPlayer;
    if (iPlayer > MAX_PLAYERS) {
        iPlayer = 0;
    }
    else if (iPlayer < 0) {
        iPlayer = MAX_PLAYERS;
    }
    while (ENTITY::DOES_ENTITY_EXIST(PLAYER::GET_PLAYER_PED_SCRIPT_INDEX(iPlayer)) == FALSE)
    {
        if (iPlayer > MAX_PLAYERS)
        {
            iPlayer = 0;
            break;
        }
        else if (iPlayer < 0)
        {
            iPlayer = MAX_PLAYERS;
            break;
        }
        direction ? iPlayer++ : iPlayer--;
    }
    if (iPlayer != iOriginalPlayer)
        ReleaseKeys(); //This is so you don't continue to blow up the server under someone's name if they leave.
}

static void BlameExplosionOnPed(Ped victim, Ped target, float x = 0.0f, float y = 0.0f, float z = 0.0f)
{
    if ((x + y + z) == 0.0f)
    {
        Vector3 pos = ENTITY::GET_ENTITY_COORDS(target, FALSE);
        x = pos.x;
        y = pos.y;
        z = pos.z;
    }
    *(PWORD)gOffsets.dwAddOwnedExplosionBypass = 0xE990;
    FIRE::ADD_OWNED_EXPLOSION(victim, x, y, z, EXPLOSION_STICKYBOMB, 10.0f, FALSE, TRUE, 0.0f);
    *(PWORD)gOffsets.dwAddOwnedExplosionBypass = 0x850F;
}

static char* GetPlayerHeadshot(Player playerParam, char* playerName = NULL)
{
    const int index = SCORE_BOARD_HEADSHOT_GLOBAL;
    char* chLogo = XorStr("CHAR_MULTIPLAYER");
    for (int i = 0; i <= 150; i += 5)
    {
        __int64* base = ENGINE::GET_GLOBAL_PTR(index + i);
        __int64 playerId = *base;
        if (playerId == -1)
            break;
        if (playerId == playerParam || (playerName != NULL && (strcmp(playerName, PLAYER::GET_PLAYER_NAME(playerId)) == 0)))
        {
            __int64* logo = ENGINE::GET_GLOBAL_PTR(index + i + 1);
            chLogo = PED::GET_PEDHEADSHOT_TXD_STRING(*logo);
            break;
        }
    }
    return chLogo;
}

static bool validateArg2(int arg)
{
    return (arg < 0 || arg > 31);
}

static bool validateStruct(uint64_t* argStruct, int start, int end)
{
    for (int i = start; i <= end; i++)
    {
        if (argStruct[i] == 0)
        {
            return true;
        }
    }
    return false;
}

static Object ptr_to_handle(void* pointer)
{
    typedef Object(__fastcall*ptr_to_handle_t)(void* pointer);
    static ptr_to_handle_t ptr_to_handleOrig = (ptr_to_handle_t)gOffsets.ptrToHandleAddress;
    return ptr_to_handleOrig(pointer);
}

typedef struct
{
    __int64 firstFaceShape, secondFaceShape, thirdFaceShape;
    __int64 firstSkinTone, secondSkinTone, thirdSkinTone;
    double parentFaceShapePercent, parentSkinTonePercent, parentThirdUnkPercent;
    __int64 isParentInheritance;
} headBlendData;
static_assert (sizeof(headBlendData) == 0x50, "headBlendData is not sized properly.");

static int getHeadData(Ped pedParam)
{
    headBlendData blendData;
    PED::_GET_PED_HEAD_BLEND_DATA(pedParam, (Any*)&blendData);
    return blendData.firstFaceShape + blendData.secondFaceShape + blendData.thirdFaceShape;
}

static void dumpOutfitData(Ped ped)
{
    pLog->WriteToFile("Component 0: %i Texture: %i", PED::GET_PED_DRAWABLE_VARIATION(ped, 0), PED::GET_PED_TEXTURE_VARIATION(ped, 0));
    pLog->WriteToFile("Component 1: %i Texture: %i", PED::GET_PED_DRAWABLE_VARIATION(ped, 1), PED::GET_PED_TEXTURE_VARIATION(ped, 1));
    pLog->WriteToFile("Component 2: %i Texture: %i", PED::GET_PED_DRAWABLE_VARIATION(ped, 2), PED::GET_PED_TEXTURE_VARIATION(ped, 2));
    pLog->WriteToFile("Component 3: %i Texture: %i", PED::GET_PED_DRAWABLE_VARIATION(ped, 3), PED::GET_PED_TEXTURE_VARIATION(ped, 3));
    pLog->WriteToFile("Component 4: %i Texture: %i", PED::GET_PED_DRAWABLE_VARIATION(ped, 4), PED::GET_PED_TEXTURE_VARIATION(ped, 4));
    pLog->WriteToFile("Component 5: %i Texture: %i", PED::GET_PED_DRAWABLE_VARIATION(ped, 5), PED::GET_PED_TEXTURE_VARIATION(ped, 5));
    pLog->WriteToFile("Component 6: %i Texture: %i", PED::GET_PED_DRAWABLE_VARIATION(ped, 6), PED::GET_PED_TEXTURE_VARIATION(ped, 6));
    pLog->WriteToFile("Component 7: %i Texture: %i", PED::GET_PED_DRAWABLE_VARIATION(ped, 7), PED::GET_PED_TEXTURE_VARIATION(ped, 7));
    pLog->WriteToFile("Component 8: %i Texture: %i", PED::GET_PED_DRAWABLE_VARIATION(ped, 8), PED::GET_PED_TEXTURE_VARIATION(ped, 8));
    pLog->WriteToFile("Component 9: %i Texture: %i", PED::GET_PED_DRAWABLE_VARIATION(ped, 9), PED::GET_PED_TEXTURE_VARIATION(ped, 9));
    pLog->WriteToFile("Component 10: %i Texture: %i", PED::GET_PED_DRAWABLE_VARIATION(ped, 10), PED::GET_PED_PROP_TEXTURE_INDEX(ped, 10));
    pLog->WriteToFile("Component 11: %i Texture: %i", PED::GET_PED_DRAWABLE_VARIATION(ped, 11), PED::GET_PED_PROP_TEXTURE_INDEX(ped, 11));
    pLog->WriteToFile("Prop 0: %i Texture: %i", PED::GET_PED_PROP_INDEX(ped, 0), PED::GET_PED_PROP_TEXTURE_INDEX(ped, 0));
    pLog->WriteToFile("Prop 1: %i Texture: %i", PED::GET_PED_PROP_INDEX(ped, 1), PED::GET_PED_PROP_TEXTURE_INDEX(ped, 1));
    pLog->WriteToFile("Prop 2: %i Texture: %i", PED::GET_PED_PROP_INDEX(ped, 2), PED::GET_PED_PROP_TEXTURE_INDEX(ped, 2));
    pLog->WriteToFile("Prop 3: %i Texture: %i", PED::GET_PED_PROP_INDEX(ped, 3), PED::GET_PED_PROP_TEXTURE_INDEX(ped, 3));
}